﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TheBooks_Model
{
    public class NhanVien_Model
    {
        [JsonProperty("MaNV")]
        public string MaNV { get; set; }

        [JsonProperty("HoTen")]
        public string HoTen { get; set; }

        [JsonProperty("Phai")]
        public string Phai { get; set; }

        [JsonProperty("NgaySinh")]
        public string NgaySinh { get; set; }

        [JsonProperty("DiaChi")]
        public string DiaChi { get; set; }

        [JsonProperty("Email")]
        public string Email { get; set; }

        [JsonProperty("Sdt")]
        public string Sdt { get; set; }

        [JsonProperty("phanconggiao")]
        public string phanconggiao { get; set; }

        [JsonProperty("phancongthu")]
        public string phancongthu { get; set; }
    }

    public class NhomQuyen_Model
    {
        [JsonProperty("MaNQ")]
        public string MaNQ { get; set; }
    }

    public class InsertNhanVien_Request
    {
        [JsonProperty("UserName")]
        public string UserName { get; set; }

        [JsonProperty("Ho")]
        public string Ho { get; set; }

        [JsonProperty("Ten")]
        public string Ten { get; set; }

        [JsonProperty("Password")]
        public string Password { get; set; }
        
        [JsonProperty("NgaySinh")]
        public string NgaySinh { get; set; }
        
        [JsonProperty("Email")]
        public string Email { get; set; }

        [JsonProperty("Sdt")]
        public string Sdt { get; set; }

        [JsonProperty("MaNQ")]
        public string MaNQ { get; set; }

        [JsonProperty("gioitinh")]
        public string gioitinh { get; set; }

        [JsonProperty("diachi")]
        public string diachi { get; set; }
    }

    public class Parent_NhanVien_Model
    {
        [JsonProperty("code")]
        public int code { get; set; }

        [JsonProperty("message")]
        public string message { get; set; }

        [JsonProperty("data")]
        public List<NhanVien_Model> data { get; set; }
    }

   
    //PHÂN CÔNG LẠI NHÂN VIÊN GIAO PHIẾU
    public class NhanVien_Request
    {
        [JsonProperty("MaPhieu")]
        public string MaPhieu { get; set; }

        [JsonProperty("MaSach")]
        public string MaSach { get; set; }

        [JsonProperty("MaNVMoi")]
        public string MaNVMoi { get; set; }
    }

}
