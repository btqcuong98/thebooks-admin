﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheBooks_Model
{
    public class KhuVucVC_Request
    {
        [JsonProperty("MaNV")]
        public string MaNV { get; set; }
    }

    public class KhuVucVC_Model
    {
        [JsonProperty("MaKV")]
        public string MaKV { get; set; }

        [JsonProperty("TenKV")]
        public string TenKV { get; set; }
    }

    public class Parent_KhuVucVC_Model
    {
        [JsonProperty("code")]
        public int code { get; set; }

        [JsonProperty("message")]
        public string message { get; set; }

        [JsonProperty("data")]
        public List<KhuVucVC_Model> data { get; set; }
    }
}
