﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TheBooks_Model
{

    //MARK : - DAUSACH
    public class DauSach_Model
    {

        [JsonProperty("MaDS")]
        public string MaDS { get; set; }

        [JsonProperty("Ten")]
        public string Ten { get; set; }

        [JsonProperty("MoTa")]
        public string MoTa { get; set; }

        [JsonProperty("GiaSach")]
        public string GiaSach { get; set; }

        [JsonProperty("solanmuon")]
        public string solanmuon { get; set; }
    }

    public class All_DauSach_Model
    {

        [JsonProperty("MaDS")]
        public string MaDS { get; set; }

        [JsonProperty("Ten")]
        public string Ten { get; set; }

        [JsonProperty("MoTa")]
        public string MoTa { get; set; }

        [JsonProperty("SoLuongTon")]
        public int SoLuongTon { get; set; }
    }


    //MARK: - SACH

    public class Sach_Request
    {

        [JsonProperty("MaDS")]
        public string MaDS { get; set; }
    }

    public class Sach_Model
    {

        [JsonProperty("MaS")]
        public string MaS { get; set; }

        [JsonProperty("ViTri")]
        public string ViTri { get; set; }

        [JsonProperty("ChuaMuon")]
        public string ChuaMuon { get; set; }
    }

    //MARK: - SACH CUA DOCGIA

    public class Sach_DocGia_Request
    {

        [JsonProperty("MaDG")]
        public string MaDG { get; set; }
    }

    public class Sach_DocGia_Model
    {

        [JsonProperty("MaS")]
        public string MaS { get; set; }

        [JsonProperty("Ten")]
        public string Ten { get; set; }

        [JsonProperty("TenTLC")]
        public string TenTLC { get; set; }

        [JsonProperty("TenNXB")]
        public string TenNXB { get; set; }

        [JsonProperty("HuyMuon")]
        public string HuyMuon { get; set; }

        [JsonProperty("DaTra")]
        public string DaTra { get; set; }

        [JsonProperty("YeuCauTra")]
        public string YeuCauTra { get; set; }

        [JsonProperty("DaGiao")]
        public string DaGiao { get; set; }

        [JsonProperty("TrangThai")]
        public string TrangThai { get; set; }
    }

    //MARK : - Thể Loại Con

    public class TheLoaiCon_Model
    {
        [JsonProperty("MaTLC")]
        public string MaTLC { get; set; }

        [JsonProperty("TenTLC")]
        public string TenTLC { get; set; }
    }


    //MARK : - PARENT

    public class Parent_DauSach_Model
    {
        [JsonProperty("code")]
        public int code { get; set; }

        [JsonProperty("message")]
        public string message { get; set; }

        [JsonProperty("data")]
        public List<DauSach_Model> data { get; set; }
    }

    public class Parent_All_DauSach_Model
    {
        [JsonProperty("code")]
        public int code { get; set; }

        [JsonProperty("message")]
        public string message { get; set; }

        [JsonProperty("data")]
        public List<All_DauSach_Model> data { get; set; }
    }

    public class Parent_Sach_Model
    {
        [JsonProperty("code")]
        public int code { get; set; }

        [JsonProperty("message")]
        public string message { get; set; }

        [JsonProperty("data")]
        public List<Sach_Model> data { get; set; }
    }

    public class Parent_Sach_DocGia_Model
    {
        [JsonProperty("code")]
        public int code { get; set; }

        [JsonProperty("message")]
        public string message { get; set; }

        [JsonProperty("data")]
        public List<Sach_DocGia_Model> data { get; set; }
    }

    public class Parent_TheLoaiCon_Model
    {
        [JsonProperty("code")]
        public int code { get; set; }

        [JsonProperty("message")]
        public string message { get; set; }

        [JsonProperty("data")]
        public List<TheLoaiCon_Model> data { get; set; }
    }


}
