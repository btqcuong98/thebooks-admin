﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TheBooks_Model
{
    public class TimKiemGiao_Model
    {
        [JsonProperty("MaPM")]
        public string MaPM { get; set; }
        
        [JsonProperty("MaNV")]
        public string MaNV { get; set; }

        [JsonProperty("HoTen")]
        public string HoTen { get; set; }

        [JsonProperty("TenNguoiNhan")]
        public string TenNguoiNhan { get; set; }

        [JsonProperty("DiaChiNhan")]
        public string DiaChiNhan { get; set; }

        [JsonProperty("SdtNhan")]
        public String SdtNhan { get; set; }

        [JsonProperty("MaS")]
        public String MaS { get; set; }

        [JsonProperty("TenS")]
        public String Ten { get; set; }

        [JsonProperty("TenTLC")]
        public String TenTLC { get; set; }

        [JsonProperty("TenNXB")]
        public String TenNXB { get; set; }

        [JsonProperty("DaGiao")]
        public String DaGiao { get; set; }
    }


    public class TimKiemThu_Model
    {

        [JsonProperty("MaPM")]
        public string MaPM { get; set; }
        
        [JsonProperty("TenNguoiNhan")]
        public string TenNguoiNhan { get; set; }

        [JsonProperty("DiaChiNhan")]
        public string DiaChiNhan { get; set; }

        [JsonProperty("SdtNhan")]
        public string SdtNhan { get; set; }

        [JsonProperty("MaNV")]
        public string MaNV { get; set; }

        [JsonProperty("HoTen")]
        public String HoTen { get; set; }

        [JsonProperty("MaS")]
        public String MaS { get; set; }

        [JsonProperty("TenS")]
        public String Ten { get; set; }

        [JsonProperty("DaTra")]
        public String DaTra { get; set; }
    }

    public class Parent_TimKiemGiao_Model
    {
        [JsonProperty("code")]
        public int code { get; set; }

        [JsonProperty("message")]
        public string message { get; set; }

        [JsonProperty("data")]
        public List<TimKiemGiao_Model> data { get; set; }
    }

    public class Parent_TimKiemThu_Model
    {
        [JsonProperty("code")]
        public int code { get; set; }

        [JsonProperty("message")]
        public string message { get; set; }

        [JsonProperty("data")]
        public List<TimKiemThu_Model> data { get; set; }
    }

    //Request

    public class TimKiem_Request
    {
        public int TrangThai { get; set; }
    }

  
    
}
