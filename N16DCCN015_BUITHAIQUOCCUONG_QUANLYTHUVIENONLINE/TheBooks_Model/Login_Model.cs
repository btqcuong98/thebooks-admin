﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TheBooks_Model
{
    public class Login_Request
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }


    public class Login_Model
    {
        [JsonProperty("MaND")]
        public string MaND { get; set; }

        [JsonProperty("HoTen")]
        public string HoTen { get; set; }

        [JsonProperty("DiaChi")]
        public string DiaChi { get; set; }

        [JsonProperty("NgaySinh")]
        public string NgaySinh { get; set; }

        [JsonProperty("Email")]
        public string Email { get; set; }

        [JsonProperty("Sdt")]
        public string Sdt { get; set; }

        [JsonProperty("MaNQ")]
        public string MaNQ { get; set; }
    }

    public class Parent_Login_Model
    {
        [JsonProperty("code")]
        public int code { get; set; }

        [JsonProperty("message")]
        public string message { get; set; }

        [JsonProperty("data")]
        public List<Login_Model> data { get; set; }
    }
}
