﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TheBooks_Model
{
    public class ViTri_Model
    {
        [JsonProperty("MaVT")]
        public string MaVT { get; set; }

        [JsonProperty("ViTri")]
        public string ViTri { get; set; }
    }

    public class Parent_ViTri_Model
    {
        [JsonProperty("code")]
        public int code { get; set; }

        [JsonProperty("message")]
        public string message { get; set; }

        [JsonProperty("data")]
        public List<ViTri_Model> data { get; set; }
    }


    //
    public class CapNhatViTri_Request
    {
        [JsonProperty("MaVT")]
        public string MaVT { get; set; }

        [JsonProperty("MaS")]
        public string MaS { get; set; }
    }
}
