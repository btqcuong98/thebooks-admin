﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TheBooks_Model
{
    public class TaiKhoan_Model
    {

        [JsonProperty("UserName")]
        public string UserName { get; set; }

        [JsonProperty("Password")]
        public string Password { get; set; }

        [JsonProperty("Email")]
        public string Email { get; set; }

        [JsonProperty("MaNQ")]
        public string MaNQ { get; set; }


        [JsonProperty("HoTen")]
        public string HoTen { get; set; }

        [JsonProperty("Phai")]
        public string Phai { get; set; }

        [JsonProperty("NgaySinh")]
        public string NgaySinh { get; set; }

        [JsonProperty("DiaChi")]
        public string DiaChi { get; set; }

        [JsonProperty("Sdt")]
        public string Sdt { get; set; }

        //request

        [JsonProperty("Ho")]
        public string Ho { get; set; }

        [JsonProperty("Ten")]
        public string Ten { get; set; }

        [JsonProperty("GioiTinh")]
        public string GioiTinh { get; set; }

        [JsonProperty("ad_hoac_thuthu")]
        public string ad_hoac_thuthu { get; set; }

        [JsonProperty("idKhungLePhi")]
        public int idKhungLePhi { get; set; }
    }

    public class Parent_TaiKhoan_Model
    {
        [JsonProperty("code")]
        public int code { get; set; }

        [JsonProperty("message")]
        public string message { get; set; }

        [JsonProperty("data")]
        public List<TaiKhoan_Model> data { get; set; }
    }
}
