﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TheBooks_Model
{
    public class NhapSach_Request
    {
        public List<String> MaNXB { get; set; }
        public List<String> MaPN { get; set; }
        public List<String> MaTT { get; set; }
        public List<String> MaDS { get; set; }
        public List<String> TenSach { get; set; }
        public List<String> TenTLC { get; set; }
        public List<String> NamXB { get; set; }
        public List<String> SoTrang { get; set; }
        public List<String> MoTa { get; set; }
        public List<String> SoLuong { get; set; }
        public List<String> Gia { get; set; }
        
    }

    public class Nhap1Sach_Request
    {
        public String MaNXB { get; set; }
        public String MaPN { get; set; }
        public String MaTT { get; set; }
        public String MaDS { get; set; }
        public String TenSach { get; set; }
        public String TenTLC { get; set; }
        public String NamXB { get; set; }
        public String SoTrang { get; set; }
        public String MoTa { get; set; }
        public String SoLuong { get; set; }
        public String Gia { get; set; }

    }

    public class Parent_NhapSach_Model
    {
        [JsonProperty("code")]
        public int code { get; set; }

        [JsonProperty("message")]
        public string message { get; set; }
    }
}
