﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TheBooks_Model
{
    public class Excel_Model
    {

        [JsonProperty("Stt")]
        public string Stt { get; set; }

        [JsonProperty("MÃ ĐẦU SÁCH")]
        public string MaDS { get; set; }

        [JsonProperty("TÊN SÁCH")]
        public string TenSach { get; set; }

        [JsonProperty("THỂ LOẠI SÁCH")]
        public string TenTLC { get; set; }

        [JsonProperty("NĂM XUẤT BẢN")]
        public string NamXB { get; set; }

        [JsonProperty("SỐ TRANG")]
        public string SoTrang { get; set; }

        [JsonProperty("MÔ TẢ")]
        public string MoTa { get; set; }

        [JsonProperty("SỐ LƯỢNG")]
        public string SoLuong { get; set; }

        [JsonProperty("GIÁ")]
        public string Gia { get; set; }
    }
    
}
