﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TheBooks_Model
{
    public class Phieu_Model
    {
        [JsonProperty("Id")]
        public string Id { get; set; }

        [JsonProperty("MaPM")]
        public string MaPM { get; set; }

        //cua giao
        [JsonProperty("MaNV")]
        public string MaNV { get; set; }

        [JsonProperty("HoTen")]
        public string HoTen { get; set; }
        //

        [JsonProperty("TenNguoiNhan")]
        public string TenNguoiNhan { get; set; }

        [JsonProperty("DiaChiNhan")]
        public string DiaChiNhan { get; set; }

        [JsonProperty("SdtNhan")]
        public String SdtNhan { get; set; }

        //[JsonProperty("DaGiao")]
        //public String DaGiao { get; set; }

        [JsonProperty("DaTra")]
        public String DaTra { get; set; }
    }

    public class Parent_Phieu_Model
    {
        [JsonProperty("code")]
        public int code { get; set; }

        [JsonProperty("message")]
        public string message { get; set; }

        [JsonProperty("data")]
        public List<Phieu_Model> data { get; set; }
    }


    //CT_PM MODEL

    public class CTPhieu_Request
    {
        public string maphieu { get; set; }
        public string manhanvienvc { get; set; }
    }

    public class CTPhieu_Model
    {
        [JsonProperty("MaS")]
        public string MaS { get; set; }

        [JsonProperty("Ten")]
        public string Ten { get; set; }

        //cua giao
        [JsonProperty("TenTLC")]
        public string TenTLC { get; set; }

        [JsonProperty("TenNXB")]
        public string TenNXB { get; set; }

        [JsonProperty("DaGiao")]
        public String DaGiao { get; set; }

        //cuar thu

        [JsonProperty("MaNV")]
        public string MaNV { get; set; }

        [JsonProperty("HoTen")]
        public string HoTen { get; set; }

        [JsonProperty("DaTra")]
        public String DaTra { get; set; }

    }

    public class Parent_CTPhieu_Model
    {
        [JsonProperty("code")]
        public int code { get; set; }

        [JsonProperty("message")]
        public string message { get; set; }

        [JsonProperty("data")]
        public List<CTPhieu_Model> data { get; set; }
    }
    
}
