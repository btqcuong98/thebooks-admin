﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TheBooks_Model
{

    //MARK : - NXB
    public class NhaXuatBan_Model
    {

        [JsonProperty("MaNXB")]
        public string MaNXB { get; set; }

        [JsonProperty("Ten")]
        public string Ten { get; set; }
    }


    //MARK : - PARENT

    public class Parent_NhaXuatBan_Model
    {
        [JsonProperty("code")]
        public int code { get; set; }

        [JsonProperty("message")]
        public string message { get; set; }

        [JsonProperty("data")]
        public List<NhaXuatBan_Model> data { get; set; }
    }
}
