﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TheBooks_Model
{
    public class TheLoaiSach
    {
        [JsonProperty("MaTLS")]
        public string MaTLS { get; set; }

        [JsonProperty("TenTLS")]
        public string TenTLS { get; set; }

    }
}
