﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TheBooks_Model
{

    public class DongLePhi_Request
    {

        [JsonProperty("MaDG")]
        public string MaDG { get; set; }

        [JsonProperty("MaLP")] //mã lệ phí
        public string MaLP { get; set; }
    }

        public class DocGia_Model
    {

        [JsonProperty("MaDG")]
        public string MaDG { get; set; }

        [JsonProperty("HoTen")]
        public string HoTen { get; set; }

        [JsonProperty("Phai")]
        public string Phai { get; set; }

        [JsonProperty("DiaChi")]
        public string DiaChi { get; set; }

        [JsonProperty("NgaySinh")]
        public string NgaySinh { get; set; }

        [JsonProperty("Email")]
        public string Email { get; set; }

        [JsonProperty("Sdt")]
        public string Sdt { get; set; }
    }

    public class Parent_DocGia_Model
    {
        [JsonProperty("code")]
        public int code { get; set; }

        [JsonProperty("message")]
        public string message { get; set; }

        [JsonProperty("data")]
        public List<DocGia_Model> data { get; set; }
    }
}
