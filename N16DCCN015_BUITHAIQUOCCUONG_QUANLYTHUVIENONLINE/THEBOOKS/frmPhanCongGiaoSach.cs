﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Net.Http;
using Newtonsoft.Json;
using TheBooks_Model;

namespace THEBOOKS
{
    public partial class frmPhanCongGiaoSach : Form
    {
        //MARK : - KHAIBAO
        List<Phieu_Model> lstPhieu = new List<Phieu_Model>();
        List<CTPhieu_Model> lstCTPhieu = new List<CTPhieu_Model>();
        List<NhanVien_Model> lstNhanVien = new List<NhanVien_Model>();
        List<TimKiemGiao_Model> lstTimKiemGiao = new List<TimKiemGiao_Model>();
        String searchPlaceholder = "Tìm theo nhân viên ....";
        Boolean isTimKiem = false;

        int row = -1;
        int rowCT = -1;

        public frmPhanCongGiaoSach()
        {
            InitializeComponent();

            txbSearch.Font = new Font(new Font("Times New Roman", 12f), FontStyle.Italic);
            txbSearch.ForeColor = Color.Gray;
            txbSearch.Text = searchPlaceholder;

            loadTatCaNhanVienToCombobox();
            loadTatCaPhieuGiao();

            if (Program.mGroup == "Admin")
            {
                groupBox1.Enabled = btnEdit.Enabled = btnSave.Enabled = false;

            }

            viewDidLoad();
        }
        
        //MARK : - CALL DATA

        private async void loadTatCaNhanVienToCombobox()
        {
            try
            {
                var httpClient = new HttpClient();
                var url = Program.requestCon + "/nhanvientt/laytatcathongtinnhanviengiao";
                var task = await httpClient.PostAsync(string.Format(url), null);
                task.EnsureSuccessStatusCode();
                var payload = task.Content.ReadAsStringAsync();

                lstNhanVien = JsonConvert.DeserializeObject<Parent_NhanVien_Model>(payload.Result).data;
                
                if (lstNhanVien.Count > 0)
                {
                    cmbMaNV.DataSource = lstNhanVien;
                    cmbMaNV.DisplayMember = "phanconggiao";
                    cmbMaNV.ValueMember = "MaNV";
                    txbMaNV.Text = cmbMaNV.SelectedValue.ToString().Trim();
                }
                else
                {
                    txbMaNV.Text = "";
                    return;
                }

            }
            catch (Exception ex)
            {
                txbMaNV.Text = "";
                return;
            }
        }

        private async void loadTatCaPhieuGiao()
        {
            try
            {
                var httpClient = new HttpClient();
                var url = Program.requestCon + "/nhanvientt/laytatcacongviecgiaosachcuatatcanv";
                var task = await httpClient.PostAsync(string.Format(url), null);
                task.EnsureSuccessStatusCode();
                var payload = task.Content.ReadAsStringAsync();

                lstPhieu = JsonConvert.DeserializeObject<Parent_Phieu_Model>(payload.Result).data;

                if (lstPhieu.Count > 0)
                {
                    dataGridView1.Invoke((MethodInvoker)delegate {
                        
                        DataTable dataTable = new DataTable();
                        dataTable.Columns.Add("MaPM");
                        dataTable.Columns.Add("MaNV");
                        dataTable.Columns.Add("HoTen");
                        dataTable.Columns.Add("TenNguoiNhan");
                        dataTable.Columns.Add("DiaChiNhan");
                        dataTable.Columns.Add("SdtNhan");
                        DataRow dr;
                        for (int i = 0; i < lstPhieu.Count; i++)
                        {
                            dr = dataTable.NewRow();
                            dr["MaPM"] = lstPhieu[i].MaPM;
                            dr["MaNV"] = lstPhieu[i].MaNV;
                            dr["HoTen"] = lstPhieu[i].HoTen;
                            dr["TenNguoiNhan"] = lstPhieu[i].TenNguoiNhan;
                            dr["DiaChiNhan"] = lstPhieu[i].DiaChiNhan;
                            dr["SdtNhan"] = lstPhieu[i].SdtNhan;
                                
                            dataTable.Rows.Add(dr);
                        }
                        dataGridView1.DataSource = dataTable;
 
                    });

                    if(row > -1)
                    {
                        dataGridView1.Rows[row].Selected = true;
                        row = -1;
                    }
                }
                else
                {
                    return;

                }
            }
            catch (Exception ex)
            {
                return;
            }
        }

        private async void loadCT(String maphieu)
        {
            try
            {
                CTPhieu_Request request = new CTPhieu_Request();
                request.maphieu = maphieu;

                HttpContent val = new StringContent(JsonConvert.SerializeObject(request), System.Text.Encoding.UTF8, "application/json");
                var url = Program.requestCon + "/nhanvientt/laytatcachitietgiaosachcuaphieugiao";
                var httpClient = new HttpClient();
                var task = await httpClient.PostAsync(string.Format(url), val);
                task.EnsureSuccessStatusCode();

                var payload = task.Content.ReadAsStringAsync();

                lstCTPhieu = JsonConvert.DeserializeObject<Parent_CTPhieu_Model>(payload.Result).data;

                if (lstCTPhieu.Count > 0)
                {
                    dataGridView2.Invoke((MethodInvoker)delegate {
                        
                        DataTable dataTable = new DataTable();
                        dataTable.Columns.Add("MaS");
                        dataTable.Columns.Add("Ten");
                        dataTable.Columns.Add("TenTLC");
                        dataTable.Columns.Add("TenNXB");
                        dataTable.Columns.Add("DaGiao");
                        DataRow dr;
                        for (int i = 0; i < lstCTPhieu.Count; i++)
                        {
                            dr = dataTable.NewRow();
                            dr["MaS"] = lstCTPhieu[i].MaS;
                            dr["Ten"] = lstCTPhieu[i].Ten;
                            dr["TenTLC"] = lstCTPhieu[i].TenTLC;
                            dr["TenNXB"] = lstCTPhieu[i].TenNXB;
                            if (lstCTPhieu[i].DaGiao == "1")
                            {
                                dr["DaGiao"] = "Đã giao";
                            }
                            else
                            {
                                dr["DaGiao"] = "Chưa giao";
                            }
                            dataTable.Rows.Add(dr);
                        }
                        dataGridView2.DataSource = dataTable;

                    });
                
                }
                else
                {
                    return;
                }

            }
            catch (Exception ex)
            {
                return;
            }

        }

        //MARK: BUTTON

        private async void btnSearch_Click(object sender, EventArgs e)
        {
            if (txbSearch.Text.Trim() != "")
            {
                DataTable dataTable = new DataTable();
                dataTable.Columns.Add("MaPM");
                dataTable.Columns.Add("MaNV");
                dataTable.Columns.Add("HoTen");
                dataTable.Columns.Add("TenNguoiNhan");
                dataTable.Columns.Add("DiaChiNhan");
                dataTable.Columns.Add("SdtNhan");
                DataRow dr;

                for (int i = 0; i < lstPhieu.Count; i++)
                {
                    if (Program.removeAccent(lstPhieu[i].MaNV).Contains(Program.removeAccent(txbSearch.Text.Trim())) ||
                        Program.removeAccent(lstPhieu[i].HoTen).Contains(Program.removeAccent(txbSearch.Text.Trim())))
                    {

                        dr = dataTable.NewRow();
                        dr["MaPM"] = lstPhieu[i].MaPM;
                        dr["MaNV"] = lstPhieu[i].MaNV;
                        dr["HoTen"] = lstPhieu[i].HoTen;
                        dr["TenNguoiNhan"] = lstPhieu[i].TenNguoiNhan;
                        dr["DiaChiNhan"] = lstPhieu[i].DiaChiNhan;
                        dr["SdtNhan"] = lstPhieu[i].SdtNhan;

                        dataTable.Rows.Add(dr);
                    }
                }
                dataGridView1.DataSource = dataTable;
                dataGridView2.DataSource = tempDataTable();
            }
        }

        private void btnReLoad_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            isTimKiem = false;
            loadTatCaPhieuGiao();
            loadTatCaNhanVienToCombobox();
            txbSearch.Text = "";
            viewDidLoad();
        }

        private void btnEdit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if(rowCT > -1)
            {
                if (dataGridView2.CurrentRow.Cells["DaGiao"].Value.ToString().Trim() == "Chưa giao")
                {
                    btnEdit_Click();
                }
                else
                {
                    MessageBox.Show("Sách đã hoàn tất giao dịch, không thể chỉnh sửa", "THÔNG BÁO", MessageBoxButtons.OK);
                    viewDidLoad();
                    return;
                }
            }
            else
            {
                MessageBox.Show("Vui lòng chọn dòng cần sửa ở phần chi tiết sách giao !", "THÔNG BÁO", MessageBoxButtons.OK);
                viewDidLoad();
                return;
            }
        }

        private async void btnSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            String maphieu = dataGridView1.CurrentRow.Cells["MaPM"].Value.ToString().Trim();

            try
            {
                NhanVien_Request request = new NhanVien_Request();
                request.MaPhieu = maphieu;
                request.MaNVMoi = txbMaNV.Text.ToString().Trim();

                HttpContent val = new StringContent(JsonConvert.SerializeObject(request), System.Text.Encoding.UTF8, "application/json");
                var url = Program.requestCon + "/nhanvientt/capnhatlainhanviengiaosach";
                var httpClient = new HttpClient();
                var task = await httpClient.PostAsync(string.Format(url), val);
                task.EnsureSuccessStatusCode();
                var payload = task.Content.ReadAsStringAsync();

                MessageBox.Show("Chỉnh sửa thành công", "THÔNG BÁO", MessageBoxButtons.OK);

                loadTatCaPhieuGiao();

                viewDidLoad();
            }
            catch
            {
                viewDidLoad();
                return;
            }
        }

        //MARK: - ACTION


        private void cmbMaNV_SelectedIndexChanged(object sender, EventArgs e)
        {
            txbMaNV.Text = cmbMaNV.SelectedValue.ToString();
        }

        private void dgvData_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            dataGridView1.CurrentRow.Selected = true;

            row = dataGridView1.CurrentCell.RowIndex;

            //combobox
            String manv = dataGridView1.CurrentRow.Cells["MaNV"].Value.ToString().Trim();
            
            //subview
            String maphieugiao = dataGridView1.CurrentRow.Cells["MaPM"].Value.ToString().Trim();

            if (manv != "" && maphieugiao != "")
            {
                cmbMaNV.SelectedValue = manv;

                if (isTimKiem == false)
                {
                    loadCT(maphieugiao);
                }
                else
                {
                    DataTable dataTable = new DataTable();
                    dataTable.Columns.Add("MaS");
                    dataTable.Columns.Add("Ten");
                    dataTable.Columns.Add("TenTLC");
                    dataTable.Columns.Add("TenNXB");
                    dataTable.Columns.Add("DaGiao");
                    DataRow dr;

                    for (int i = 0; i < lstTimKiemGiao.Count; i++)
                    {
                        if (lstTimKiemGiao[i].MaPM == maphieugiao)
                        {
                            dr = dataTable.NewRow();
                            dr["MaS"] = lstTimKiemGiao[i].MaS;
                            dr["Ten"] = lstTimKiemGiao[i].Ten;
                            dr["TenTLC"] = lstTimKiemGiao[i].TenTLC;
                            dr["TenNXB"] = lstTimKiemGiao[i].TenNXB;
                            dr["DaGiao"] = lstTimKiemGiao[i].DaGiao;
                            dataTable.Rows.Add(dr);
                        }
                    }
                    dataGridView2.DataSource = dataTable;
                }
            }
        }

        private void datagridview2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            dataGridView2.CurrentRow.Selected = true;
            rowCT = dataGridView2.CurrentCell.RowIndex;
        }

        //MARK : - FUNC 

        //tùy biến nút lệnh
        private void viewDidLoad()
        {
            btnSave.Enabled = false;
            groupBox1.Enabled = false;
            btnEdit.Enabled = true;
        }

        private void btnEdit_Click()
        {
            btnSave.Enabled = true;
            groupBox1.Enabled = true;
            btnEdit.Enabled = false;
        }
        
        private void txbSearch_MouseEnter(object sender, EventArgs e)
        {
            if(txbSearch.Text == searchPlaceholder)
            {
                txbSearch.Text = "";
                txbSearch.Font = new Font(new Font("Times New Roman", 14f), FontStyle.Regular);
                txbSearch.ForeColor = Color.Black;
            }
            
        }

        private void txbSearch_MouseLeave(object sender, EventArgs e)
        {
            if(txbSearch.Text == "")
            {
                txbSearch.Font = new Font(new Font("Times New Roman", 12f), FontStyle.Italic);
                txbSearch.ForeColor = Color.Gray;
                txbSearch.Text = searchPlaceholder;
            }
            
        }

        private async void ChuaGiao_Click(object sender, EventArgs e)
        {
            isTimKiem = true;
            try { 
                DataTable dataTable = new DataTable();

                TimKiem_Request request = new TimKiem_Request();
                request.TrangThai = 0; 

                HttpContent val = new StringContent(JsonConvert.SerializeObject(request), System.Text.Encoding.UTF8, "application/json");
                var url = Program.requestCon + "/nhanvientt/timkiemtheotrangthaisachgiao";
                var httpClient = new HttpClient();
                var task = await httpClient.PostAsync(string.Format(url), val);
                task.EnsureSuccessStatusCode();
                var payload = task.Content.ReadAsStringAsync();

                lstTimKiemGiao = JsonConvert.DeserializeObject<Parent_TimKiemGiao_Model>(payload.Result).data;

                dataTable.Columns.Add("MaPM");
                dataTable.Columns.Add("MaNV");
                dataTable.Columns.Add("HoTen");
                dataTable.Columns.Add("TenNguoiNhan");
                dataTable.Columns.Add("DiaChiNhan");
                dataTable.Columns.Add("SdtNhan");
                DataRow dr;

                for (int i = 0; i < lstTimKiemGiao.Count; i++)
                {
                    if(i > 0 && lstTimKiemGiao[i].MaPM != lstTimKiemGiao[i-1].MaPM)
                    {
                        dr = dataTable.NewRow();
                        dr["MaPM"] = lstTimKiemGiao[i].MaPM;
                        dr["MaNV"] = lstTimKiemGiao[i].MaNV;
                        dr["HoTen"] = lstTimKiemGiao[i].HoTen;
                        dr["TenNguoiNhan"] = lstTimKiemGiao[i].TenNguoiNhan;
                        dr["DiaChiNhan"] = lstTimKiemGiao[i].DiaChiNhan;
                        dr["SdtNhan"] = lstTimKiemGiao[i].SdtNhan;
                        dataTable.Rows.Add(dr);
                    }
                    else if(i == 0)
                    {
                        dr = dataTable.NewRow();
                        dr["MaPM"] = lstTimKiemGiao[0].MaPM;
                        dr["MaNV"] = lstTimKiemGiao[0].MaNV;
                        dr["HoTen"] = lstTimKiemGiao[0].HoTen;
                        dr["TenNguoiNhan"] = lstTimKiemGiao[0].TenNguoiNhan;
                        dr["DiaChiNhan"] = lstTimKiemGiao[0].DiaChiNhan;
                        dr["SdtNhan"] = lstTimKiemGiao[0].SdtNhan;
                        dataTable.Rows.Add(dr);
                    }
                }
                dataGridView1.DataSource = dataTable;
                dataGridView2.DataSource = tempDataTable();
            }
            catch (Exception ex)
            {
                return;
            }
        }

        private async void DaGiao_Click(object sender, EventArgs e)
        {
            isTimKiem = true;
            try
            {
                DataTable dataTable = new DataTable();

                TimKiem_Request request = new TimKiem_Request();
                request.TrangThai = 1;

                HttpContent val = new StringContent(JsonConvert.SerializeObject(request), System.Text.Encoding.UTF8, "application/json");
                var url = Program.requestCon + "/nhanvientt/timkiemtheotrangthaisachgiao";
                var httpClient = new HttpClient();
                var task = await httpClient.PostAsync(string.Format(url), val);
                task.EnsureSuccessStatusCode();
                var payload = task.Content.ReadAsStringAsync();

                lstTimKiemGiao = JsonConvert.DeserializeObject<Parent_TimKiemGiao_Model>(payload.Result).data;

                dataTable.Columns.Add("MaPM");
                dataTable.Columns.Add("MaNV");
                dataTable.Columns.Add("HoTen");
                dataTable.Columns.Add("TenNguoiNhan");
                dataTable.Columns.Add("DiaChiNhan");
                dataTable.Columns.Add("SdtNhan");
                DataRow dr;

                for (int i = 0; i < lstTimKiemGiao.Count; i++)
                {
                    if (i > 0 && lstTimKiemGiao[i].MaPM != lstTimKiemGiao[i - 1].MaPM)
                    {
                        dr = dataTable.NewRow();
                        dr["MaPM"] = lstTimKiemGiao[i].MaPM;
                        dr["MaNV"] = lstTimKiemGiao[i].MaNV;
                        dr["HoTen"] = lstTimKiemGiao[i].HoTen;
                        dr["TenNguoiNhan"] = lstTimKiemGiao[i].TenNguoiNhan;
                        dr["DiaChiNhan"] = lstTimKiemGiao[i].DiaChiNhan;
                        dr["SdtNhan"] = lstTimKiemGiao[i].SdtNhan;
                        dataTable.Rows.Add(dr);
                    }
                    else if(i == 0)
                    {
                        dr = dataTable.NewRow();
                        dr["MaPM"] = lstTimKiemGiao[0].MaPM;
                        dr["MaNV"] = lstTimKiemGiao[0].MaNV;
                        dr["HoTen"] = lstTimKiemGiao[0].HoTen;
                        dr["TenNguoiNhan"] = lstTimKiemGiao[0].TenNguoiNhan;
                        dr["DiaChiNhan"] = lstTimKiemGiao[0].DiaChiNhan;
                        dr["SdtNhan"] = lstTimKiemGiao[0].SdtNhan;
                        dataTable.Rows.Add(dr);
                    }
                }
                dataGridView1.DataSource = dataTable;
                dataGridView2.DataSource = tempDataTable();
            }
            catch (Exception ex)
            {
                return;
            }
        }

        private DataTable tempDataTable()
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add("MaS");
            dataTable.Columns.Add("Ten");
            dataTable.Columns.Add("TenTLC");
            dataTable.Columns.Add("TenNXB");
            dataTable.Columns.Add("DaGiao");
            return dataTable;
        }
    }
}
