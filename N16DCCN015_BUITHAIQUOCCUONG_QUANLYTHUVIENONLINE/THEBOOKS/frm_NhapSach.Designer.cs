﻿namespace THEBOOKS
{
    partial class frm_NhapSach
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txbMaDS = new System.Windows.Forms.TextBox();
            this.txbTenS = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txbMaTL = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txbSoTrang = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btnAddBook = new System.Windows.Forms.Button();
            this.cmbTheLoai = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txbMoTa = new System.Windows.Forms.TextBox();
            this.txbGia = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cmbNXB = new System.Windows.Forms.ComboBox();
            this.txbMaNXB = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txbMaPN = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txbNXB = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(311, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(163, 31);
            this.label1.TabIndex = 0;
            this.label1.Text = "NHẬP SÁCH";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.ForeColor = System.Drawing.Color.LawnGreen;
            this.label2.Location = new System.Drawing.Point(89, 115);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 21);
            this.label2.TabIndex = 1;
            this.label2.Text = "Mã Đầu Sách";
            // 
            // txbMaDS
            // 
            this.txbMaDS.Location = new System.Drawing.Point(232, 114);
            this.txbMaDS.Name = "txbMaDS";
            this.txbMaDS.Size = new System.Drawing.Size(159, 29);
            this.txbMaDS.TabIndex = 2;
            // 
            // txbTenS
            // 
            this.txbTenS.Location = new System.Drawing.Point(232, 163);
            this.txbTenS.Name = "txbTenS";
            this.txbTenS.Size = new System.Drawing.Size(316, 29);
            this.txbTenS.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.ForeColor = System.Drawing.Color.LawnGreen;
            this.label3.Location = new System.Drawing.Point(120, 165);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 21);
            this.label3.TabIndex = 3;
            this.label3.Text = "Tên Sách";
            // 
            // txbMaTL
            // 
            this.txbMaTL.Location = new System.Drawing.Point(569, 212);
            this.txbMaTL.Name = "txbMaTL";
            this.txbMaTL.ReadOnly = true;
            this.txbMaTL.Size = new System.Drawing.Size(124, 29);
            this.txbMaTL.TabIndex = 6;
            this.txbMaTL.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.ForeColor = System.Drawing.Color.LawnGreen;
            this.label4.Location = new System.Drawing.Point(82, 215);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(119, 21);
            this.label4.TabIndex = 5;
            this.label4.Text = "Thể Loại Sách";
            // 
            // txbSoTrang
            // 
            this.txbSoTrang.Location = new System.Drawing.Point(232, 318);
            this.txbSoTrang.Name = "txbSoTrang";
            this.txbSoTrang.Size = new System.Drawing.Size(316, 29);
            this.txbSoTrang.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.ForeColor = System.Drawing.Color.LawnGreen;
            this.label6.Location = new System.Drawing.Point(122, 323);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(79, 21);
            this.label6.TabIndex = 9;
            this.label6.Text = "Số Trang";
            // 
            // btnAddBook
            // 
            this.btnAddBook.BackColor = System.Drawing.Color.Maroon;
            this.btnAddBook.ForeColor = System.Drawing.Color.LavenderBlush;
            this.btnAddBook.Location = new System.Drawing.Point(232, 488);
            this.btnAddBook.Name = "btnAddBook";
            this.btnAddBook.Size = new System.Drawing.Size(316, 39);
            this.btnAddBook.TabIndex = 11;
            this.btnAddBook.Text = "LƯU DỮ LIỆU";
            this.btnAddBook.UseVisualStyleBackColor = false;
            this.btnAddBook.Click += new System.EventHandler(this.btnAddBook_Click);
            // 
            // cmbTheLoai
            // 
            this.cmbTheLoai.FormattingEnabled = true;
            this.cmbTheLoai.Location = new System.Drawing.Point(232, 212);
            this.cmbTheLoai.Name = "cmbTheLoai";
            this.cmbTheLoai.Size = new System.Drawing.Size(316, 29);
            this.cmbTheLoai.TabIndex = 12;
            this.cmbTheLoai.SelectedIndexChanged += new System.EventHandler(this.cmbTheLoai_SelectedIndexChanged);
            this.cmbTheLoai.MouseClick += new System.Windows.Forms.MouseEventHandler(this.cmbTheLoai_MouseClick);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.ForeColor = System.Drawing.Color.LawnGreen;
            this.label7.Location = new System.Drawing.Point(147, 373);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(54, 21);
            this.label7.TabIndex = 13;
            this.label7.Text = "Mô tả";
            // 
            // txbMoTa
            // 
            this.txbMoTa.Location = new System.Drawing.Point(232, 367);
            this.txbMoTa.Name = "txbMoTa";
            this.txbMoTa.Size = new System.Drawing.Size(316, 29);
            this.txbMoTa.TabIndex = 14;
            // 
            // txbGia
            // 
            this.txbGia.Location = new System.Drawing.Point(232, 417);
            this.txbGia.Name = "txbGia";
            this.txbGia.Size = new System.Drawing.Size(316, 29);
            this.txbGia.TabIndex = 18;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.ForeColor = System.Drawing.Color.LawnGreen;
            this.label9.Location = new System.Drawing.Point(165, 420);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(36, 21);
            this.label9.TabIndex = 17;
            this.label9.Text = "Giá";
            // 
            // cmbNXB
            // 
            this.cmbNXB.FormattingEnabled = true;
            this.cmbNXB.Location = new System.Drawing.Point(232, 264);
            this.cmbNXB.Name = "cmbNXB";
            this.cmbNXB.Size = new System.Drawing.Size(316, 29);
            this.cmbNXB.TabIndex = 21;
            this.cmbNXB.SelectedIndexChanged += new System.EventHandler(this.cmbTenNXB_SelectedIndexChanged);
            this.cmbNXB.MouseClick += new System.Windows.Forms.MouseEventHandler(this.cmbNXB_MouseClick);
            // 
            // txbMaNXB
            // 
            this.txbMaNXB.Location = new System.Drawing.Point(569, 264);
            this.txbMaNXB.Name = "txbMaNXB";
            this.txbMaNXB.ReadOnly = true;
            this.txbMaNXB.Size = new System.Drawing.Size(124, 29);
            this.txbMaNXB.TabIndex = 20;
            this.txbMaNXB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.ForeColor = System.Drawing.Color.LawnGreen;
            this.label10.Location = new System.Drawing.Point(87, 267);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(114, 21);
            this.label10.TabIndex = 19;
            this.label10.Text = "Nhà Xuất Bản";
            // 
            // txbMaPN
            // 
            this.txbMaPN.Location = new System.Drawing.Point(232, 66);
            this.txbMaPN.Name = "txbMaPN";
            this.txbMaPN.Size = new System.Drawing.Size(316, 29);
            this.txbMaPN.TabIndex = 23;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.ForeColor = System.Drawing.Color.LawnGreen;
            this.label11.Location = new System.Drawing.Point(76, 66);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(125, 21);
            this.label11.TabIndex = 22;
            this.label11.Text = "Mã Phiếu Nhập";
            // 
            // txbNXB
            // 
            this.txbNXB.Location = new System.Drawing.Point(569, 114);
            this.txbNXB.Name = "txbNXB";
            this.txbNXB.Size = new System.Drawing.Size(124, 29);
            this.txbNXB.TabIndex = 27;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.ForeColor = System.Drawing.Color.LawnGreen;
            this.label5.Location = new System.Drawing.Point(419, 118);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(119, 21);
            this.label5.TabIndex = 26;
            this.label5.Text = "Năm Xuất Bản";
            // 
            // frm_NhapSach
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::THEBOOKS.Properties.Resources.images;
            this.ClientSize = new System.Drawing.Size(732, 564);
            this.Controls.Add(this.txbNXB);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txbMaPN);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.cmbNXB);
            this.Controls.Add(this.txbMaNXB);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txbGia);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txbMoTa);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.cmbTheLoai);
            this.Controls.Add(this.btnAddBook);
            this.Controls.Add(this.txbSoTrang);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txbMaTL);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txbTenS);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txbMaDS);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.Name = "frm_NhapSach";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frm_NhapSach";
            this.Load += new System.EventHandler(this.frm_NhapSach_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txbMaDS;
        private System.Windows.Forms.TextBox txbTenS;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txbMaTL;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txbSoTrang;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnAddBook;
        private System.Windows.Forms.ComboBox cmbTheLoai;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txbMoTa;
        private System.Windows.Forms.TextBox txbGia;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cmbNXB;
        private System.Windows.Forms.TextBox txbMaNXB;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txbMaPN;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txbNXB;
        private System.Windows.Forms.Label label5;
    }
}