﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Net.Http;
using System.Windows.Forms;
using TheBooks_Model;

namespace THEBOOKS
{
    public partial class frmTaoTaiKhoan : Form
    {
        List<TaiKhoan_Model> lstTK = new List<TaiKhoan_Model>();

        public frmTaoTaiKhoan()
        {
            InitializeComponent();
            NhomQuyenToCombobox();
            loadTatCaTaiKhoan();
            groupBox2.Enabled = false;
            dteNgaySinh.Text = DateTime.Now.ToString("dd/MM/yyyy");
        }

        private void taikhoan_refresh()
        {
            txbHo.Text = "";
            txbUserName.Text = "";
            txbTen.Text = "";
            txbPassword.Text = "";
            dteNgaySinh.Text = "";
            txbEmail.Text = "";
            txbSdt.Text = "";
            txbDiaChi.Text = "";
            groupBox2.Enabled = false;
            groupBox1.Enabled = true;
        }

        private void taikhoan_them()
        {
            txbHo.Text = "";
            txbUserName.Text = "";
            txbTen.Text = "";
            txbPassword.Text = "";
            dteNgaySinh.Text = "";
            txbEmail.Text = "";
            txbSdt.Text = "";
            txbDiaChi.Text = "";
            groupBox2.Enabled = true;
            groupBox1.Enabled = false;
        }

        private bool CheckEmpty()
        {
            if(txbUserName.Text.ToString().Trim() == "")
            {
                MessageBox.Show("Không được để trống trường UserName !", "THÔNG BÁO", MessageBoxButtons.OK);
                txbUserName.Focus();
                return false;
            }
            return true;
        }

        private void NhomQuyenToCombobox()
        {
            List<NhomQuyen_Model> lstNQ = new List<NhomQuyen_Model>();

            if (Program.mGroup == "Admin")
            {
                NhomQuyen_Model nhomquyen = new NhomQuyen_Model();
                nhomquyen.MaNQ = "NVVC";

                NhomQuyen_Model nhomquyen2 = new NhomQuyen_Model();
                nhomquyen2.MaNQ = "ThuThu";

                lstNQ.Add(nhomquyen);
                lstNQ.Add(nhomquyen2);
            }
            if(Program.mGroup == "ThuThu")
            {
                NhomQuyen_Model nhomquyen = new NhomQuyen_Model();
                nhomquyen.MaNQ = "DocGia";
                lstNQ.Add(nhomquyen);
            }
            cmbNQ.DataSource = lstNQ;
            cmbNQ.DisplayMember = "MaNQ";
            cmbNQ.ValueMember = "MaNQ";
        }

        private async void loadTatCaTaiKhoan()
        {
            try
            {
                TaiKhoan_Model request = new TaiKhoan_Model();
                request.MaNQ = Program.mGroup;

                HttpContent val = new StringContent(JsonConvert.SerializeObject(request), System.Text.Encoding.UTF8, "application/json");

                var httpClient = new HttpClient();
                var url = Program.requestCon + "/taikhoan/laytatcataikhoan";
                var task = await httpClient.PostAsync(string.Format(url), val);
                task.EnsureSuccessStatusCode();
                var payload = task.Content.ReadAsStringAsync();

                lstTK = JsonConvert.DeserializeObject<Parent_TaiKhoan_Model>(payload.Result).data;

                if (lstTK.Count > 0)
                {
                    dataGridView1.Invoke((MethodInvoker)delegate {

                        DataTable dataTable = new DataTable();
                        dataTable.Columns.Add("UserName");
                        dataTable.Columns.Add("Password");
                        dataTable.Columns.Add("HoTen");
                        dataTable.Columns.Add("Phai");
                        dataTable.Columns.Add("DiaChi");
                        dataTable.Columns.Add("NgaySinh");
                        dataTable.Columns.Add("Email");
                        dataTable.Columns.Add("Sdt");
                        DataRow dr;
                        for (int i = 0; i < lstTK.Count; i++)
                        {
                            dr = dataTable.NewRow();
                            dr["UserName"] = lstTK[i].UserName;
                            dr["Password"] = lstTK[i].Password;
                            dr["HoTen"] = lstTK[i].HoTen;
                            dr["Phai"] = lstTK[i].Phai;
                            dr["DiaChi"] = lstTK[i].DiaChi;
                            dr["NgaySinh"] = Program.Format_ServerToLocal(lstTK[i].NgaySinh);
                            dr["Email"] = lstTK[i].Email;
                            dr["Sdt"] = lstTK[i].Sdt;

                            dataTable.Rows.Add(dr);
                        }
                        
                        dataGridView1.DataSource = dataTable;

                    });
                }
                else
                {
                    //MessageBox.Show("Lỗi !", "", MessageBoxButtons.OK);
                    return;

                }

            }
            catch (Exception ex)
            {
                //MessageBox.Show("Lỗi !: = "+ ex.ToString(), "", MessageBoxButtons.OK);
                return;

            }
        }


        private async void btnSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (!CheckEmpty())
            {
                return;
            }

            try
            {
                TaiKhoan_Model request = new TaiKhoan_Model();
                request.UserName = txbUserName.Text.ToString().Trim();
                request.Ho = txbHo.Text.ToString().Trim();
                request.Ten = txbTen.Text.ToString().Trim();
                request.Password = txbPassword.Text.ToString().Trim();
                request.NgaySinh = Program.FormatLocalToServer(dteNgaySinh.Text);
                request.Email = txbEmail.Text.ToString().Trim();
                request.Sdt = txbSdt.Text.ToString().Trim();
                request.MaNQ = cmbNQ.SelectedValue.ToString().Trim();
                request.GioiTinh = cmbGioiTinh.Text.ToString().Trim();
                request.DiaChi = txbDiaChi.Text.ToString().Trim();
                request.ad_hoac_thuthu = Program.mGroup;
                if(cmbLePhi.Text.ToString().Trim() == "Lệ Phí Theo Tháng")
                {
                    request.idKhungLePhi = 1;
                }
                else if (cmbLePhi.Text.ToString().Trim() == "Lệ Phí Theo Năm")
                {
                    request.idKhungLePhi = 2;
                }

                HttpContent val = new StringContent(JsonConvert.SerializeObject(request), System.Text.Encoding.UTF8, "application/json");
                var url = Program.requestCon + "/taikhoan/taotaikhoan";

                var httpClient = new HttpClient();
                var task = await httpClient.PostAsync(string.Format(url), val);
                task.EnsureSuccessStatusCode();
                
                MessageBox.Show("Tạo Tài Khoản Thành Công !", "THÔNG BÁO", MessageBoxButtons.OK);

                loadTatCaTaiKhoan();
                taikhoan_refresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Tạo Tài Khoản Thất Bại !"+ ex.ToString(), "THÔNG BÁO", MessageBoxButtons.OK);
                taikhoan_refresh();
                return;
            }
        }

        private void btnReLoad_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            loadTatCaTaiKhoan();
            NhomQuyenToCombobox();
            txbSearch.Text = "";
            taikhoan_refresh();
        }

        private void datagridview1_cellClick(object sender, DataGridViewCellEventArgs e)
        {
            dataGridView1.CurrentRow.Selected = true;
            
            txbUserName.Text = dataGridView1.CurrentRow.Cells["UserName"].Value.ToString().Trim();
            string[] nameList = dataGridView1.CurrentRow.Cells["HoTen"].Value.ToString().Trim().Split(' ');
            string ho = "";
            for (int i = 0; i < nameList.Length - 1; i++)
            {
                ho += nameList[i] + " ";
            }
            txbHo.Text = Program.chuanhoachuoi(ho);
            txbTen.Text = Program.chuanhoachuoi(nameList[nameList.Length - 1]);
            txbDiaChi.Text = dataGridView1.CurrentRow.Cells["DiaChi"].Value.ToString().Trim();
            txbEmail.Text = dataGridView1.CurrentRow.Cells["Email"].Value.ToString().Trim();
            txbSdt.Text = dataGridView1.CurrentRow.Cells["Sdt"].Value.ToString().Trim();
        }

        private void btnAdd_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            groupBox2.Enabled = true;
            groupBox1.Enabled = false;
            taikhoan_them();
        }

        private void btnSearch_Click_1(object sender, EventArgs e)
        {
            if (txbSearch.Text.Trim() != "")
            {
                DataTable dataTable = new DataTable();
                dataTable.Columns.Add("UserName");
                dataTable.Columns.Add("Password");
                dataTable.Columns.Add("HoTen");
                dataTable.Columns.Add("Phai");
                dataTable.Columns.Add("DiaChi");
                dataTable.Columns.Add("NgaySinh");
                dataTable.Columns.Add("Email");
                dataTable.Columns.Add("Sdt");
                DataRow dr;

                for (int i = 0; i < lstTK.Count; i++)
                {
                    if (Program.removeAccent(lstTK[i].UserName).Contains(Program.removeAccent(txbSearch.Text.Trim())) ||
                        Program.removeAccent(lstTK[i].HoTen).Contains(Program.removeAccent(txbSearch.Text.Trim())))
                    {

                        dr = dataTable.NewRow();
                        dr["UserName"] = lstTK[i].UserName;
                        dr["Password"] = lstTK[i].Password;
                        dr["HoTen"] = lstTK[i].HoTen;
                        dr["Phai"] = lstTK[i].Phai;
                        dr["DiaChi"] = lstTK[i].DiaChi;
                        dr["NgaySinh"] = lstTK[i].NgaySinh;
                        dr["Email"] = lstTK[i].Email;
                        dr["Sdt"] = lstTK[i].Sdt;

                        dataTable.Rows.Add(dr);
                    }
                }
                dataGridView1.DataSource = dataTable;
            }
        }
    }
}
