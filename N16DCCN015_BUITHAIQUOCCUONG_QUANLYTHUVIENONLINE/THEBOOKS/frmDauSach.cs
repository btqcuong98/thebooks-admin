﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TheBooks_Model;
using System.Net.Http;
using Newtonsoft.Json;


namespace THEBOOKS
{
    public partial class frmDauSach : Form
    {
        //MARK : - KHAI BAO

        Parent_All_DauSach_Model businessunits = new Parent_All_DauSach_Model();
        List<All_DauSach_Model> lstSearch = new List<All_DauSach_Model>();
        List<Sach_Model> lstSach = new List<Sach_Model>();
        String madausach = "";
        String masach = "";
        int row = -1;
        int rowSub = -1;

        public frmDauSach()
        {
            InitializeComponent();
            load();
            loadViTriToCombobox();
            groupBox4.Enabled = false;
        }

        //MARK : - CALL DATA

        private async void load()
        {

            try
            {
                var httpClient = new HttpClient();
                var url = Program.requestCon + "/dausach/tatcadausach";
                var task = await httpClient.PostAsync(string.Format(url), null);//.ConfigureAwait(false);

                task.EnsureSuccessStatusCode();

                var payload = task.Content.ReadAsStringAsync();
          
                businessunits = JsonConvert.DeserializeObject<Parent_All_DauSach_Model>(payload.Result);


                if (businessunits.data.Count > 0)
                {

                    //Main f = new Main();
                    //this.Hide();
                    //f.ShowDialog();
                    dataGridView1.Invoke((MethodInvoker)delegate { dataGridView1.DataSource = businessunits.data; });
                }
                else
                {
                    //MessageBox.Show("thất bại !", "", MessageBoxButtons.OK);
                    return;

                }

            }
            catch (Exception ex)
            {
                //MessageBox.Show("thất bại !", "", MessageBoxButtons.OK);
                return;

            }

        }

        private async void loadSachCuaDauSach()
        {
            try
            {
                Sach_Request request = new Sach_Request();
                request.MaDS = madausach;
                HttpContent val = new StringContent(JsonConvert.SerializeObject(request), System.Text.Encoding.UTF8, "application/json");
                var httpClient = new HttpClient();
                var url = Program.requestCon + "/dausach/laytatsasachcudausach";
                var task = await httpClient.PostAsync(string.Format(url), val);//.ConfigureAwait(false);

                task.EnsureSuccessStatusCode();

                var payload = task.Content.ReadAsStringAsync(); 
                lstSach = JsonConvert.DeserializeObject<Parent_Sach_Model>(payload.Result).data;


                if (lstSach.Count > 0)
                {
                    
                    dataGridView2.Invoke((MethodInvoker)delegate {
                        for (int i = 0; i < lstSach.Count; i++)
                        {
                            if(lstSach[i].ChuaMuon == "1")
                            {
                                lstSach[i].ChuaMuon = "Chưa mượn";
                            }
                            if (lstSach[i].ChuaMuon == "0")
                            {
                                lstSach[i].ChuaMuon = "Đã mượn";
                            }
                        }
                        dataGridView2.DataSource = lstSach;
                    });
                }
                else
                {
                    //MessageBox.Show("thất bại !", "", MessageBoxButtons.OK);
                    return;

                }

            }
            catch (Exception ex)
            {
                //MessageBox.Show("thất bại !", "", MessageBoxButtons.OK);
                return;

            }

        }

        private async void loadViTriToCombobox()
        {
            try
            {
                var httpClient = new HttpClient();
                var url = Program.requestCon + "/vitri/laytatcavitri";
                var task = await httpClient.PostAsync(string.Format(url), null);//.ConfigureAwait(false);
                task.EnsureSuccessStatusCode();
                var payload = task.Content.ReadAsStringAsync();

                List<ViTri_Model> lstViTri = new List<ViTri_Model>();
                lstViTri = JsonConvert.DeserializeObject<Parent_ViTri_Model>(payload.Result).data;

                cmbViTri.DataSource = lstViTri;
                cmbViTri.DisplayMember = "ViTri";
                cmbViTri.ValueMember = "MaVT";
            }
            catch (Exception ex)
            {
                //MessageBox.Show("thất bại !", "", MessageBoxButtons.OK);
                return;

            }

        }

        //MARK : - BUTTON

        private void btnAll_Click(object sender, EventArgs e)
        {
            load();
            textBox1.Text = "";
            lstSearch.Clear();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Trim() != "")
            {
                for (int i = 0; i < businessunits.data.Count; i++)
                {
                    All_DauSach_Model item = new All_DauSach_Model();
                    if (Program.removeAccent(businessunits.data[i].MaDS).Contains(Program.removeAccent(textBox1.Text.Trim())) ||
                        Program.removeAccent(businessunits.data[i].Ten).Contains(Program.removeAccent(textBox1.Text.Trim())))
                    {
                        item = businessunits.data[i];
                        lstSearch.Add(item);
                    }
                }
                dataGridView1.DataSource = lstSearch;
                dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            }
            else
            {
                lstSearch.Clear();
                dataGridView1.DataSource = businessunits.data;
            }
        }

        private async void btnSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if(masach != "" && cmbViTri.SelectedValue.ToString() != null)
            {
                try
                {
                    CapNhatViTri_Request request = new CapNhatViTri_Request();
                    request.MaS = masach;
                    request.MaVT = cmbViTri.SelectedValue.ToString();

                    HttpContent val = new StringContent(JsonConvert.SerializeObject(request), System.Text.Encoding.UTF8, "application/json");
                    var url = Program.requestCon + "/vitri/capnhatvitri";
                    var httpClient = new HttpClient();
                    var task = await httpClient.PostAsync(string.Format(url), val);
                    task.EnsureSuccessStatusCode();

                    var payload = task.Content.ReadAsStringAsync();

                    dataGridView2.Rows[rowSub].Cells[1].Value = cmbViTri.Text.ToString();
                    MessageBox.Show("Cập nhật vị trí thành công !", "THÔNG BÁO", MessageBoxButtons.OK);
                    groupBox4.Enabled = false;
                }
                catch (Exception ex)
                {
                    return;
                }
            }
        }

        private void btnReLoad_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            load();
            textBox1.Text = "";
            lstSearch.Clear();
            loadViTriToCombobox();
            groupBox4.Enabled = false;
        }

        private void datagridview1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            dataGridView1.CurrentRow.Selected = true;
            row = dataGridView1.CurrentCell.RowIndex;
            madausach = dataGridView1.CurrentRow.Cells["MaDS"].Value.ToString().Trim();
            loadSachCuaDauSach();
        }

        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            dataGridView2.CurrentRow.Selected = true;
            rowSub = dataGridView2.CurrentCell.RowIndex;
            masach = dataGridView2.CurrentRow.Cells["MaS"].Value.ToString().Trim();
            groupBox4.Enabled = true;
        }

        private void btnAddBook_Click(object sender, EventArgs e)
        {
            frm_NhapSach f = new frm_NhapSach();
            f.Show();
        }

        private void cmbViTri_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
