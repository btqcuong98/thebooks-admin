﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TheBooks_Model;

namespace THEBOOKS
{
    public partial class frmQuanLyDocGia : Form
    {

        //MARK : - KHAIBAO
        List<DocGia_Model> lstDocGia = new List<DocGia_Model>();
        List<Sach_DocGia_Model> lstSach = new List<Sach_DocGia_Model>();
        List<Sach_DocGia_Model> lstSachTemp = new List<Sach_DocGia_Model>();
        String madocgia = "";
        int row = -1;

        public frmQuanLyDocGia()
        {
            InitializeComponent();

            loadTatCaDocGia();
        }

        //MARK : - CALL DATA

        private async void loadTatCaDocGia()
        {
            try
            {
                var httpClient = new HttpClient();
                var url = Program.requestCon + "/nhanvientt/laytatcadocgia";
                var task = await httpClient.PostAsync(string.Format(url), null);//.ConfigureAwait(false);

                task.EnsureSuccessStatusCode();

                var payload = task.Content.ReadAsStringAsync();

                lstDocGia = JsonConvert.DeserializeObject<Parent_DocGia_Model>(payload.Result).data;


                if (lstDocGia.Count > 0)
                {
                    dataGridView1.Invoke((MethodInvoker)delegate {
                        dataGridView1.DataSource = lstDocGia;
                    });
                }
                else
                {
                    //MessageBox.Show("thất bại !", "", MessageBoxButtons.OK);
                    return;

                }

            }
            catch (Exception ex)
            {
                //MessageBox.Show("thất bại !", "", MessageBoxButtons.OK);
                return;

            }

        }

        private async void loadTatCaSachCuaDocGia()
        {
            try
            {
                Sach_DocGia_Request request = new Sach_DocGia_Request();
                request.MaDG = madocgia;
                HttpContent val = new StringContent(JsonConvert.SerializeObject(request), System.Text.Encoding.UTF8, "application/json");
                var httpClient = new HttpClient();
                var url = Program.requestCon + "/docgia/laytatcasachcuadocgia";
                var task = await httpClient.PostAsync(string.Format(url), val);//.ConfigureAwait(false);

                task.EnsureSuccessStatusCode();

                var payload = task.Content.ReadAsStringAsync();

                lstSach = JsonConvert.DeserializeObject<Parent_Sach_DocGia_Model>(payload.Result).data;

                DataTable dataTable = new DataTable();
                dataTable.Columns.Add("MaS");
                dataTable.Columns.Add("Ten");
                dataTable.Columns.Add("TenTLC");
                dataTable.Columns.Add("TenNXB");
                dataTable.Columns.Add("TrangThai");
                DataRow dr;
                for (int i = 0; i < lstSach.Count; i++)
                {
                    dr = dataTable.NewRow();

                    dr["MaS"] = lstSach[i].MaS;
                    dr["Ten"] = lstSach[i].Ten;
                    dr["TenTLC"] = lstSach[i].TenTLC;
                    dr["TenNXB"] = lstSach[i].TenNXB;

                    if (lstSach[i].HuyMuon == "1")
                    {
                        dr["TrangThai"] = "Đã hủy";
                    }
                    else if (lstSach[i].YeuCauTra == "1")
                    {
                        dr["TrangThai"] = "Yêu cầu trả";
                    }
                    else if (lstSach[i].DaTra == "1")
                    {
                        dr["TrangThai"] = "Đã Trả";
                    }
                    else if (lstSach[i].DaGiao == "1")
                    {
                        dr["TrangThai"] = "Đang mượn";
                    }
                    else
                    {
                        dr["TrangThai"] = "Chờ giao";
                    }

                    dataTable.Rows.Add(dr);
                }
                dataGridView2.DataSource = dataTable;
            }
            catch (Exception ex)
            {
                //MessageBox.Show("thất bại !", "", MessageBoxButtons.OK);
                return;

            }

        }


        //MARK : - BUTTON
        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (txbSearch.Text.Trim() != "")
            {
                DataTable dataTable = new DataTable();
                dataTable.Columns.Add("MaDG");
                dataTable.Columns.Add("HoTen");
                dataTable.Columns.Add("Phai");
                dataTable.Columns.Add("DiaChi");
                dataTable.Columns.Add("NgaySinh");
                dataTable.Columns.Add("Email");
                dataTable.Columns.Add("Sdt");
                DataRow dr;
                for (int i = 0; i < lstDocGia.Count; i++)
                {
                    if (Program.removeAccent(lstDocGia[i].MaDG).Contains(Program.removeAccent(txbSearch.Text.Trim())) ||
                        Program.removeAccent(lstDocGia[i].HoTen).Contains(Program.removeAccent(txbSearch.Text.Trim())))
                    {
                        dr = dataTable.NewRow();
                        dr["MaDG"] = lstDocGia[i].MaDG;
                        dr["HoTen"] = lstDocGia[i].HoTen;
                        dr["Phai"] = lstDocGia[i].Phai;
                        dr["DiaChi"] = lstDocGia[i].DiaChi;
                        dr["NgaySinh"] = lstDocGia[i].NgaySinh;
                        dr["Email"] = lstDocGia[i].Email;
                        dr["Sdt"] = lstDocGia[i].Sdt;
                        dataTable.Rows.Add(dr);
                    }
                }
               dataGridView1.DataSource = dataTable;
            }
        }

        private void datagridview1_CellCllick(object sender, DataGridViewCellEventArgs e)
        {
            dataGridView1.CurrentRow.Selected = true;
            row = dataGridView1.CurrentCell.RowIndex;
            madocgia = dataGridView1.CurrentRow.Cells["MaDG"].Value.ToString().Trim();
            loadTatCaSachCuaDocGia();
        }

        private void btnRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            loadTatCaDocGia();
        }

        private void DongLePhi_Click(object sender, EventArgs e)
        {
            frmDongLePhi f = new frmDongLePhi();
            f.ShowDialog();
        }

        private void dgv2_Click(object sender, DataGridViewCellEventArgs e)
        {
            dataGridView2.CurrentRow.Selected = true;
        }
    }
}
