﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TheBooks_Model;
using System.Net.Http;
using Newtonsoft.Json;

namespace THEBOOKS
{
    public partial class frmPhanCongThuSach : Form
    {
        List<Phieu_Model> lstPhieu = new List<Phieu_Model>();
        List<CTPhieu_Model> lstCtPhieu = new List<CTPhieu_Model>();
        List<NhanVien_Model> lstAllNhanVien = new List<NhanVien_Model>();
        List<TimKiemThu_Model> lstTimKiemThu = new List<TimKiemThu_Model>();
        String searchPlaceholder = "Tìm theo thông tin người nhận ....";

        Boolean isTimKiem = false;
        int row = -1;
        int rowsub = -1;

        public frmPhanCongThuSach()
        {
            InitializeComponent();

            txbSearch.Font = new Font(new Font("Times New Roman", 12f), FontStyle.Italic);
            txbSearch.ForeColor = Color.Gray;
            txbSearch.Text = searchPlaceholder;

            loadTatCaNhanVienToCombox();
            loadTatCaPhieuCuaNhanVien();

            if (Program.mGroup == "Admin")
            {
                groupBox1.Enabled = btnSave.Enabled = false;

            }

            viewDidLoad();
        }

        //MARK: - FUNC

        //tùy biến nút lệnh
        private void viewDidLoad()
        {
            btnSave.Enabled = false;
            groupBox1.Enabled = false;
        }

        private void subRowSelected()
        {
            btnSave.Enabled = true;
            groupBox1.Enabled = true;
        }

        private async void loadTatCaNhanVienToCombox()
        {
            try
            {
                var url = Program.requestCon + "/nhanvientt/laytatcathongtinnhanvienthu";
                var httpClient = new HttpClient();
                var task = await httpClient.PostAsync(string.Format(url), null);
                task.EnsureSuccessStatusCode();
                var payload = task.Content.ReadAsStringAsync();

                lstAllNhanVien = JsonConvert.DeserializeObject<Parent_NhanVien_Model>(payload.Result).data;


                if (lstAllNhanVien.Count > 0)
                {
                    cmbMaNV.DataSource = lstAllNhanVien;
                    cmbMaNV.DisplayMember = "phancongthu";
                    cmbMaNV.ValueMember = "MaNV";

                    txbMaNV.Text = cmbMaNV.SelectedValue.ToString().Trim();
                }
                else
                {
                    return;

                }

            }
            catch (Exception ex)
            {
                return;

            }
        }

        private async void loadTatCaPhieuCuaNhanVien()
        {
            try
            {
                var url = Program.requestCon + "/nhanvientt/laytatcacongviecthusachcuatatcanv";
                var httpClient = new HttpClient();
                var task = await httpClient.PostAsync(string.Format(url), null);
                task.EnsureSuccessStatusCode();
                var payload = task.Content.ReadAsStringAsync();

                lstPhieu = JsonConvert.DeserializeObject<Parent_Phieu_Model>(payload.Result).data;

                if (lstPhieu.Count > 0)
                {
                    dgvData.Invoke((MethodInvoker)delegate {

                        DataTable dataTable = new DataTable();
                        dataTable.Columns.Add("MaPM");
                        dataTable.Columns.Add("TenNguoiNhan");
                        dataTable.Columns.Add("DiaChiNhan");
                        dataTable.Columns.Add("SdtNhan");
                        DataRow dr;
                        for (int i = 0; i < lstPhieu.Count; i++)
                        {
                            dr = dataTable.NewRow();
                            dr["MaPM"] = lstPhieu[i].MaPM;
                            dr["TenNguoiNhan"] = lstPhieu[i].TenNguoiNhan;
                            dr["DiaChiNhan"] = lstPhieu[i].DiaChiNhan;
                            dr["SdtNhan"] = lstPhieu[i].SdtNhan;
                            
                            dataTable.Rows.Add(dr);
                        }
                        dgvData.DataSource = dataTable;

                    });
                }
                else
                {
                    MessageBox.Show("Không có dữ liệu !", "", MessageBoxButtons.OK);
                    return;

                }

            }
            catch (Exception ex)
            {
                //MessageBox.Show("Load dữ liệu thất bại !", "", MessageBoxButtons.OK);
                return;

            }

        }

        private async void loadCTPhieu(String maphieu)
        {
        
            try
            {
                CTPhieu_Request request = new CTPhieu_Request();
                request.maphieu = maphieu;

                HttpContent val = new StringContent(JsonConvert.SerializeObject(request), System.Text.Encoding.UTF8, "application/json");
                var url = Program.requestCon + "/nhanvientt/laytatcachitietthusachcuaphieuthu";
                var httpClient = new HttpClient();
                var task = await httpClient.PostAsync(string.Format(url), val);
                task.EnsureSuccessStatusCode();
                var payload = task.Content.ReadAsStringAsync();

                lstCtPhieu = JsonConvert.DeserializeObject<Parent_CTPhieu_Model>(payload.Result).data;

                DataTable dataTable = new DataTable();
                dataTable.Columns.Add("MaS");
                dataTable.Columns.Add("Ten");
                dataTable.Columns.Add("MaNV");
                dataTable.Columns.Add("HoTen");
                dataTable.Columns.Add("DaTra");
                DataRow dr;
                for (int i = 0; i < lstCtPhieu.Count; i++)
                {
                    dr = dataTable.NewRow();
                    dr["MaS"] = lstCtPhieu[i].MaS;
                    dr["Ten"] = lstCtPhieu[i].Ten;
                    dr["MaNV"] = lstCtPhieu[i].MaNV;
                    dr["HoTen"] = lstCtPhieu[i].HoTen;
                    if (lstCtPhieu[i].DaTra == "1")
                    {
                        dr["DaTra"] = "Đã thu";
                    }
                    else
                    {
                        dr["DaTra"] = "Chưa thu";
                    }
                    dataTable.Rows.Add(dr);
                }
                dataGridView2.DataSource = dataTable;

                if (rowsub > -1)
                {
                    dataGridView2.Rows[rowsub].Selected = true;
                    rowsub = -1;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Lỗi !: = " + ex.ToString(), "", MessageBoxButtons.OK);
                return;

            }

        }

        //MARK: - BUTTON

        private void btnReLoad_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            loadTatCaPhieuCuaNhanVien();
            loadTatCaNhanVienToCombox();
            txbSearch.Text = "";
            viewDidLoad();
            isTimKiem = false;
        }
        
        private void btnSerach_Click(object sender, EventArgs e)
        {
            if (txbSearch.Text.Trim() != "")
            {
                DataTable dataTable = new DataTable();
                dataTable.Columns.Add("MaPM");
                dataTable.Columns.Add("TenNguoiNhan");
                dataTable.Columns.Add("DiaChiNhan");
                dataTable.Columns.Add("SdtNhan");
                DataRow dr;

                for (int i = 0; i < lstPhieu.Count; i++)
                {
                    if (Program.removeAccent(lstPhieu[i].MaPM).Contains(Program.removeAccent(txbSearch.Text.Trim())) ||
                        Program.removeAccent(lstPhieu[i].TenNguoiNhan).Contains(Program.removeAccent(txbSearch.Text.Trim())) ||
                        Program.removeAccent(lstPhieu[i].SdtNhan).Contains(Program.removeAccent(txbSearch.Text.Trim())))
                    {
                        dr = dataTable.NewRow();
                        dr["MaPM"] = lstPhieu[i].MaPM;
                        dr["TenNguoiNhan"] = lstPhieu[i].TenNguoiNhan;
                        dr["DiaChiNhan"] = lstPhieu[i].DiaChiNhan;
                        dr["SdtNhan"] = lstPhieu[i].SdtNhan;
                        dataTable.Rows.Add(dr);
                    }
                }
                dgvData.DataSource = dataTable;
                dataGridView2.DataSource = tempDataTable();
            }
        }

        private async void btnSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            if (dataGridView2.CurrentRow.Cells["DaTra"].Value.ToString().Trim() == "Chưa thu")
            {
                row = dgvData.CurrentCell.RowIndex;
                rowsub = dataGridView2.CurrentCell.RowIndex;

                try
                {
                    string maphieu = lstPhieu[row].MaPM;
                    string masach = lstCtPhieu[rowsub].MaS;

                    var httpClient = new HttpClient();

                    NhanVien_Request request = new NhanVien_Request();
                    request.MaPhieu = maphieu;
                    request.MaSach = masach;
                    request.MaNVMoi = txbMaNV.Text.ToString().Trim();

                    HttpContent val = new StringContent(JsonConvert.SerializeObject(request), System.Text.Encoding.UTF8, "application/json");
                    var url = Program.requestCon + "/nhanvientt/capnhatlainhanvienthusach";

                    var task = await httpClient.PostAsync(string.Format(url), val);

                    task.EnsureSuccessStatusCode();

                    var payload = task.Content.ReadAsStringAsync();

                    MessageBox.Show("Chỉnh sửa thành công", "THÔNG BÁO", MessageBoxButtons.OK);
                    loadCTPhieu(maphieu);

                    viewDidLoad();
                } catch
                {
                    viewDidLoad();
                    return;
                }
                
            }
            else
            {
                viewDidLoad();
                MessageBox.Show("Sách đã hoàn tất giao dịch, không thể chỉnh sửa", "THÔNG BÁO", MessageBoxButtons.OK);
            }
        }

        //MARK: - ACTION

        private void cmbMaNV_SelectedIndexChanged(object sender, EventArgs e)
        {
            txbMaNV.Text = cmbMaNV.SelectedValue.ToString();
        }
        
        private void txbSearch_MouseEnter(object sender, EventArgs e)
        {
            if (txbSearch.Text == searchPlaceholder)
            {
                txbSearch.Text = "";
                txbSearch.Font = new Font(new Font("Times New Roman", 14f), FontStyle.Regular);
                txbSearch.ForeColor = Color.Black;
            }
        }

        private void txbSearch_MouseLeave(object sender, EventArgs e)
        {
            if (txbSearch.Text == "")
            {
                txbSearch.Font = new Font(new Font("Times New Roman", 12f), FontStyle.Italic);
                txbSearch.ForeColor = Color.Gray;
                txbSearch.Text = searchPlaceholder;
            }
        }

        private void dgv_cellClick(object sender, DataGridViewCellEventArgs e)
        {
            dgvData.CurrentRow.Selected = true;
            row = dgvData.CurrentCell.RowIndex;
            dataGridView2.Enabled = true;
            
            //subview
            String maphieu = dgvData.CurrentRow.Cells["MaPM"].Value.ToString().Trim();

            if (isTimKiem == false)
            {
                loadCTPhieu(maphieu);
            }
            else
            {
                DataTable dataTable = new DataTable();
                dataTable.Columns.Add("MaS");
                dataTable.Columns.Add("Ten");
                dataTable.Columns.Add("MaNV");
                dataTable.Columns.Add("HoTen");
                dataTable.Columns.Add("DaTra");
                DataRow dr;
                for (int i = 0; i < lstTimKiemThu.Count; i++)
                {
                    if (lstTimKiemThu[i].MaPM == maphieu)
                    {
                        dr = dataTable.NewRow();
                        dr["MaS"] = lstTimKiemThu[i].MaS;
                        dr["Ten"] = lstTimKiemThu[i].Ten;
                        dr["MaNV"] = lstTimKiemThu[i].MaNV;
                        dr["HoTen"] = lstTimKiemThu[i].HoTen;
                        dr["DaTra"] = lstTimKiemThu[i].DaTra;
                        
                        dataTable.Rows.Add(dr);
                    }
                }
                dataGridView2.DataSource = dataTable;
            }
        }

        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if(dataGridView2.CurrentRow.Cells["DaTra"].Value.ToString().Trim() == "Chưa thu") {
                subRowSelected();
            } else
            {
                viewDidLoad();
            }

            dataGridView2.CurrentRow.Selected = true;
            rowsub = dataGridView2.CurrentCell.RowIndex;
            //combobox
            if (lstCtPhieu.Count > 0)
            {
                String manv = dataGridView2.CurrentRow.Cells["MaNV"].Value.ToString().Trim();
                if(manv != "")
                {
                    cmbMaNV.SelectedValue = manv;
                }
                
            }

        }

        private async void DaThu_Click(object sender, EventArgs e)
        {
            isTimKiem = true;
            try
            {
                DataTable dataTable = new DataTable();

                TimKiem_Request request = new TimKiem_Request();
                request.TrangThai = 1;

                HttpContent val = new StringContent(JsonConvert.SerializeObject(request), System.Text.Encoding.UTF8, "application/json");
                var url = Program.requestCon + "/nhanvientt/timkiemtheotrangthaisachthu";
                var httpClient = new HttpClient();
                var task = await httpClient.PostAsync(string.Format(url), val);
                task.EnsureSuccessStatusCode();
                var payload = task.Content.ReadAsStringAsync();

                lstTimKiemThu = JsonConvert.DeserializeObject<Parent_TimKiemThu_Model>(payload.Result).data;

                dataTable.Columns.Add("MaPM");
                dataTable.Columns.Add("TenNguoiNhan");
                dataTable.Columns.Add("DiaChiNhan");
                dataTable.Columns.Add("SdtNhan");

                DataRow dr;

                for (int i = 0; i < lstTimKiemThu.Count; i++)
                {
                    if (i > 0 && lstTimKiemThu[i].MaPM != lstTimKiemThu[i - 1].MaPM)
                    {
                        dr = dataTable.NewRow();
                        dr["MaPM"] = lstTimKiemThu[i].MaPM;
                        dr["TenNguoiNhan"] = lstTimKiemThu[i].TenNguoiNhan;
                        dr["DiaChiNhan"] = lstTimKiemThu[i].DiaChiNhan;
                        dr["SdtNhan"] = lstTimKiemThu[i].SdtNhan;
                        dataTable.Rows.Add(dr);
                    }
                    else if (i == 0)
                    {
                        dr = dataTable.NewRow();
                        dr["MaPM"] = lstTimKiemThu[0].MaPM;
                        dr["TenNguoiNhan"] = lstTimKiemThu[0].TenNguoiNhan;
                        dr["DiaChiNhan"] = lstTimKiemThu[0].DiaChiNhan;
                        dr["SdtNhan"] = lstTimKiemThu[0].SdtNhan;
                        dataTable.Rows.Add(dr);
                    }
                }
                dgvData.DataSource = dataTable;
                dataGridView2.DataSource = tempDataTable();
            }
            catch (Exception ex)
            {
                return;
            }
        }

        private async void ChuaThu_Click(object sender, EventArgs e)
        {
            isTimKiem = true;
            try
            {
                DataTable dataTable = new DataTable();

                TimKiem_Request request = new TimKiem_Request();
                request.TrangThai = 0;

                HttpContent val = new StringContent(JsonConvert.SerializeObject(request), System.Text.Encoding.UTF8, "application/json");
                var url = Program.requestCon + "/nhanvientt/timkiemtheotrangthaisachthu";
                var httpClient = new HttpClient();
                var task = await httpClient.PostAsync(string.Format(url), val);
                task.EnsureSuccessStatusCode();
                var payload = task.Content.ReadAsStringAsync();

                lstTimKiemThu = JsonConvert.DeserializeObject<Parent_TimKiemThu_Model>(payload.Result).data;

                dataTable.Columns.Add("MaPM");
                dataTable.Columns.Add("TenNguoiNhan");
                dataTable.Columns.Add("DiaChiNhan");
                dataTable.Columns.Add("SdtNhan");
                
                DataRow dr;

                for (int i = 0; i < lstTimKiemThu.Count; i++)
                {
                    if (i > 0 && lstTimKiemThu[i].MaPM != lstTimKiemThu[i - 1].MaPM)
                    {
                        dr = dataTable.NewRow();
                        dr["MaPM"] = lstTimKiemThu[i].MaPM;
                        dr["TenNguoiNhan"] = lstTimKiemThu[i].TenNguoiNhan;
                        dr["DiaChiNhan"] = lstTimKiemThu[i].DiaChiNhan;
                        dr["SdtNhan"] = lstTimKiemThu[i].SdtNhan;
                        dataTable.Rows.Add(dr);
                    }
                    else if (i == 0)
                    {
                        dr = dataTable.NewRow();
                        dr["MaPM"] = lstTimKiemThu[0].MaPM;
                        dr["TenNguoiNhan"] = lstTimKiemThu[0].TenNguoiNhan;
                        dr["DiaChiNhan"] = lstTimKiemThu[0].DiaChiNhan;
                        dr["SdtNhan"] = lstTimKiemThu[0].SdtNhan;
                        dataTable.Rows.Add(dr);
                    }
                }
                dgvData.DataSource = dataTable;
                dataGridView2.DataSource = tempDataTable();
            }
            catch (Exception ex)
            {
                return;
            }
        }

        private DataTable tempDataTable()
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add("MaS");
            dataTable.Columns.Add("Ten");
            dataTable.Columns.Add("MaNV");
            dataTable.Columns.Add("HoTen");
            dataTable.Columns.Add("DaTra");
            return dataTable;
        }
    }
}
