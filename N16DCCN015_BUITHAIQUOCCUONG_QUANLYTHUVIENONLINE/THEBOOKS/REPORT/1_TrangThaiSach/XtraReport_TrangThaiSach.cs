﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace THEBOOKS.REPORT.TrangThaiSach
{
    public partial class XtraReport_TrangThaiSach : DevExpress.XtraReports.UI.XtraReport
    {
        public XtraReport_TrangThaiSach()
        {
            InitializeComponent();
            lblDateNow.Text = DateTime.Now.ToString("hh:mm - dd/MM/yyyy");

            String date = DateTime.Now.ToString("dd/MM/yyyy");
            xrNgay.Text = date.Split('/')[0].ToString();
            xrThang.Text = date.Split('/')[1].ToString();
            xrNam.Text = date.Split('/')[2].ToString();

            xrTenNhanVien.Text = Program.mHoten;
        }

    }
}
