﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace THEBOOKS.REPORT._4_LoiNhuan
{
    public partial class XtraReport_LoiNhuan : DevExpress.XtraReports.UI.XtraReport
    {
        public XtraReport_LoiNhuan(DateTime from, DateTime to)
        {
            InitializeComponent();
            lblFromDate.Text = Program.FormatDateTimeToString(from);
            lblToDate.Text = Program.FormatDateTimeToString(to);
            this.sp_Report_LoiNhuanTableAdapter1.Fill(ds1.sp_Report_LoiNhuan, from, to);

            String date = DateTime.Now.ToString("dd/MM/yyyy");
            xrNgay.Text = date.Split('/')[0].ToString();
            xrThang.Text = date.Split('/')[1].ToString();
            xrNam.Text = date.Split('/')[2].ToString();

            xrTenNhanVien.Text = Program.mHoten;
        }

    }
}
