﻿using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using THEBOOKS.REPORT._5_ThuLePhi;
using THEBOOKS.REPORT.NhapSach;

namespace THEBOOKS.REPORT.FORM._1_TuNgayDenNgay
{
    public partial class frm_TuNgayDenNgay : Form
    {
        public frm_TuNgayDenNgay()
        {
            InitializeComponent();
            dteFrom.Text = DateTime.Now.ToString("dd/MM/yyyy");
            dteTo.Text = DateTime.Now.ToString("dd/MM/yyyy");
        }

        private void btnReport_Click(object sender, EventArgs e)
        {
            if (dteFrom.Text == "" || dteTo.Text == "" || dteFrom.Text == null || dteTo.Text == null)
            {
                if (dteFrom.Text == "" || dteFrom.Text == null)
                {
                    dteFrom.Focus();
                }

                if (dteTo.Text == "" || dteTo.Text == null)
                {
                    dteTo.Focus();
                }
                MessageBox.Show("Vui lòng nhập đầy đủ các trường dữ liệu", "THÔNG BÁO", MessageBoxButtons.OK);
            } 

            if ( dteFrom.Text != "" && dteTo.Text != "" && dteFrom.Text != null && dteTo.Text != null) 
            {
                this.Enabled = false;
                if(Program.fromReport == "XtraReport_NhapSach")
                {
                    DateTime from = DateTime.Parse(dteFrom.Text);
                    DateTime to = DateTime.Parse(dteTo.Text);

                    XtraReport_NhapSach rpt = new XtraReport_NhapSach(from, to);
                    ReportPrintTool print = new ReportPrintTool(rpt);
                    print.ShowPreview();
                }
                
                if (Program.fromReport == "XtraReport_ThuLePhi")
                {
                    DateTime from = Convert.ToDateTime(dteFrom.Text);
                    DateTime to = Convert.ToDateTime(dteTo.Text);

                    XtraReport_ThuLePhi rpt = new XtraReport_ThuLePhi(from, to);
                    ReportPrintTool print = new ReportPrintTool(rpt);
                    print.ShowPreview();
                } 
                
                this.Hide();
            }
            
        }
    }
}
