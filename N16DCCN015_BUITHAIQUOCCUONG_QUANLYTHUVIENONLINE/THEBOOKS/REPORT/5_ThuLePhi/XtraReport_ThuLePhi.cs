﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Windows.Forms;

namespace THEBOOKS.REPORT._5_ThuLePhi
{
    public partial class XtraReport_ThuLePhi : DevExpress.XtraReports.UI.XtraReport
    {
        public XtraReport_ThuLePhi(DateTime from, DateTime to)
        {
            InitializeComponent();

            lblFromDate.Text = Program.FormatDateTimeToString(from);
            lblToDate.Text = Program.FormatDateTimeToString(to);
            try
            {
                this.sp_Report_ThuLePhiTableAdapter3.Fill(ds3.sp_Report_ThuLePhi, from, to);
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message, "", MessageBoxButtons.OK);
            }

            String date = DateTime.Now.ToString("dd/MM/yyyy");
            xrNgay.Text = date.Split('/')[0].ToString();
            xrThang.Text = date.Split('/')[1].ToString();
            xrNam.Text = date.Split('/')[2].ToString();

            xrTenNhanVien.Text = Program.mHoten;
        }

    }
}
