﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraReports.UI;
using MySql.Data.MySqlClient;
using THEBOOKS.REPORT._6_SoLuotMuonCuaSach;

namespace THEBOOKS
{
    public partial class frm_ReportTanSuatSach : Form
    {
        MySqlConnection connection = new MySqlConnection(Program.connectString);

        public frm_ReportTanSuatSach()
        {
            InitializeComponent();

            columnChart.Series["Số Lượt Mượn"].Points.Clear();
            pieChart.Series["Số Lượt Mượn"].Points.Clear();

            dteFrom.Text = "01/01/2020";
            dteTo.Text = DateTime.Now.ToString("dd/MM/yyyy");
            chart();
        }

        public void chart()
        {
            try
            {
                DateTime from = DateTime.Parse(dteFrom.Text);
                DateTime to = DateTime.Parse(dteTo.Text);
                connection.Open();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "CALL `sp_Report_TanSuatSachMuon`('" + Program.FormatLocalToServer(from.ToString("dd/MM/yyyy")) + "', '" + Program.FormatLocalToServer(to.ToString("dd/MM/yyyy")) + "')";
                MySqlDataReader reader;
                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    columnChart.Series["Số Lượt Mượn"].Points.AddPoint(reader.GetString("MaDS"), reader.GetInt32("SoLuotMuon"));
                    pieChart.Series["Số Lượt Mượn"].Points.AddPoint(reader.GetString("MaDS"), reader.GetInt32("SoLuotMuon"));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "", MessageBoxButtons.OK);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            columnChart.Series["Số Lượt Mượn"].Points.Clear();
            pieChart.Series["Số Lượt Mượn"].Points.Clear();
            chart();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DateTime from = Convert.ToDateTime(dteFrom.Text);
            DateTime to = Convert.ToDateTime(dteTo.Text);

            XtraReport_SoLuotMuonCuaSach rpt = new XtraReport_SoLuotMuonCuaSach(from, to);
            ReportPrintTool print = new ReportPrintTool(rpt);
            print.ShowPreview();
        }
    }
}
