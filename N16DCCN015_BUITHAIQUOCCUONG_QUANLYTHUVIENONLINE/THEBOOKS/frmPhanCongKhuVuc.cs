﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TheBooks_Model;

namespace THEBOOKS
{
    public partial class frmPhanCongKhuVuc : Form
    {
        List<NhanVien_Model> lstNhanVien = new List<NhanVien_Model>();
        List<KhuVucVC_Model> lstCtKhuVuc = new List<KhuVucVC_Model>();
        String manhanvien = "";
        int rowSub = -1;
        int row = -1;
        String searchPlaceholder = "Tìm theo thông tin nhân viên ....";
        

        public frmPhanCongKhuVuc()
        {
            InitializeComponent();

            loadNhanVientoMainView();
            //loadKhuVucToCombobox();

            txbSearch.Font = new Font(new Font("Times New Roman", 12f), FontStyle.Italic);
            txbSearch.ForeColor = Color.Gray;
            txbSearch.Text = searchPlaceholder;

            viewDidLoad();
        }

        

        //MARK : - ASYNC
        private async void loadNhanVientoMainView()
        {
            try
            {
                var httpClient = new HttpClient();
                var url = Program.requestCon + "/nhanvientt/laytatcathongtinnhanviengiao";
                var task = await httpClient.PostAsync(string.Format(url), null);
                task.EnsureSuccessStatusCode();
                var payload = task.Content.ReadAsStringAsync();

                lstNhanVien = JsonConvert.DeserializeObject<Parent_NhanVien_Model>(payload.Result).data;

                DataTable dataTable = new DataTable();
                dataTable.Columns.Add("MaNV");
                dataTable.Columns.Add("HoTen");
                dataTable.Columns.Add("Phai");
                dataTable.Columns.Add("DiaChi");
                dataTable.Columns.Add("NgaySinh");
                dataTable.Columns.Add("Email");
                dataTable.Columns.Add("Sdt");
                DataRow dr;
                for (int i = 0; i < lstNhanVien.Count; i++)
                {
                    dr = dataTable.NewRow();
                    dr["MaNV"] = lstNhanVien[i].MaNV;
                    dr["HoTen"] = lstNhanVien[i].HoTen;
                    dr["Phai"] = lstNhanVien[i].Phai;
                    dr["DiaChi"] = lstNhanVien[i].DiaChi;
                    dr["NgaySinh"] = lstNhanVien[i].NgaySinh;
                    dr["Email"] = lstNhanVien[i].Email;
                    dr["Sdt"] = lstNhanVien[i].Sdt;

                    dataTable.Rows.Add(dr);
                }
                dgvMain.DataSource = dataTable;
            }
            catch (Exception ex)
            {
                return;
            }
        }

        private async void loadKhuVuctoSubView()
        {
            try
            {
                KhuVucVC_Request request = new KhuVucVC_Request();
                request.MaNV = manhanvien;
                HttpContent val = new StringContent(JsonConvert.SerializeObject(request), System.Text.Encoding.UTF8, "application/json");
                var url = Program.requestCon + "/khuvuc/laytatcakhuvucvanchuyencuanhanvien";
                var httpClient = new HttpClient();
                var task = await httpClient.PostAsync(string.Format(url), val);
                task.EnsureSuccessStatusCode();
                var payload = task.Content.ReadAsStringAsync();

                lstCtKhuVuc = JsonConvert.DeserializeObject<Parent_KhuVucVC_Model>(payload.Result).data;

                DataTable dataTable = new DataTable();
                dataTable.Columns.Add("MaKV");
                dataTable.Columns.Add("TenKV");
                DataRow dr;
                for (int i = 0; i < lstCtKhuVuc.Count; i++)
                {
                    dr = dataTable.NewRow();
                    dr["MaKV"] = lstCtKhuVuc[i].MaKV;
                    dr["TenKV"] = lstCtKhuVuc[i].TenKV;

                    dataTable.Rows.Add(dr);
                }
                dgvSub.DataSource = dataTable;
            }
            catch (Exception ex)
            {
                return;
            }
        }

        private async void loadKhuVucToCombobox()
        {
            try
            {
                var httpClient = new HttpClient();
                var url = Program.requestCon + "/khuvuc/laytatcakhuvuc";
                var task = await httpClient.PostAsync(string.Format(url), null);
                task.EnsureSuccessStatusCode();
                var payload = task.Content.ReadAsStringAsync();

                lstCtKhuVuc = JsonConvert.DeserializeObject<Parent_KhuVucVC_Model>(payload.Result).data;

                cmbKhuVuc.DataSource = lstCtKhuVuc;
                cmbKhuVuc.DisplayMember = "TenKV";
                cmbKhuVuc.ValueMember = "MaKV";
            }
            catch(Exception ex)
            {
                return;
            }
        }

        //MARK : - FUNCTION

        private void viewDidLoad()
        {
            btnSave.Enabled = false;
            groupBox3.Enabled = false;
        }

        private void subRowSelected()
        {
            groupBox3.Enabled = true;
            btnSave.Enabled = true;
        }

        private DataTable mainTempDataTable()
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add("MaNV");
            dataTable.Columns.Add("HoTen");
            dataTable.Columns.Add("Phai");
            dataTable.Columns.Add("DiaChi");
            dataTable.Columns.Add("NgaySinh");
            dataTable.Columns.Add("Email");
            dataTable.Columns.Add("Sdt");
            return dataTable;
        }

        private DataTable subTempDataTable()
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add("MaKV");
            dataTable.Columns.Add("TenKV");
            return dataTable;
        }


        //MARK : - ACTION
        private void btnReload_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            loadNhanVientoMainView();
            //loadTatCaKhuVucVCAddCombobox();
            viewDidLoad();
        }

        private void dgvMain_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            dgvMain.CurrentRow.Selected = true;
            row = dgvMain.CurrentCell.RowIndex;
            manhanvien = dgvMain.CurrentRow.Cells["MaNV"].Value.ToString().Trim();
            loadKhuVuctoSubView();
        }

        private void dgvSub_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            dgvSub.CurrentRow.Selected = true;
            rowSub = dgvSub.CurrentCell.RowIndex;
            subRowSelected();
            //loadKhuVucToCombobox();
        }

        private void cmbKhuVuc_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void txbSearch_MouseEnter(object sender, EventArgs e)
        {
            if (txbSearch.Text == searchPlaceholder)
            {
                txbSearch.Text = "";
                txbSearch.Font = new Font(new Font("Times New Roman", 14f), FontStyle.Regular);
                txbSearch.ForeColor = Color.Black;
            }
        }

        private void txbSearch_MouseLeave(object sender, EventArgs e)
        {
            if (txbSearch.Text == "")
            {
                txbSearch.Font = new Font(new Font("Times New Roman", 12f), FontStyle.Italic);
                txbSearch.ForeColor = Color.Gray;
                txbSearch.Text = searchPlaceholder;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
       
             if (txbSearch.Text.Trim() != "")
            {
                DataTable dataTable = new DataTable();
                dataTable.Columns.Add("MaNV");
                dataTable.Columns.Add("HoTen");
                dataTable.Columns.Add("Phai");
                dataTable.Columns.Add("DiaChi");
                dataTable.Columns.Add("NgaySinh");
                dataTable.Columns.Add("Email");
                dataTable.Columns.Add("Sdt");
                DataRow dr;

                for (int i = 0; i < lstNhanVien.Count; i++)
                {

                    if (Program.removeAccent(lstNhanVien[i].MaNV).Contains(Program.removeAccent(txbSearch.Text.Trim())) ||
                        Program.removeAccent(lstNhanVien[i].HoTen).Contains(Program.removeAccent(txbSearch.Text.Trim())))
                    {

                        dr = dataTable.NewRow();
                        dr["MaNV"] = lstNhanVien[i].MaNV;
                        dr["HoTen"] = lstNhanVien[i].HoTen;
                        dr["Phai"] = lstNhanVien[i].Phai;
                        dr["DiaChi"] = lstNhanVien[i].DiaChi;
                        dr["NgaySinh"] = lstNhanVien[i].NgaySinh;
                        dr["Email"] = lstNhanVien[i].Email;
                        dr["Sdt"] = lstNhanVien[i].Sdt;

                        dataTable.Rows.Add(dr);
                    }
                }
                dgvMain.DataSource = dataTable;
            }
            
        }

        private void cmbKhuVuc_MouseClick(object sender, MouseEventArgs e)
        {
            loadKhuVucToCombobox(); 
        }
    }
}
