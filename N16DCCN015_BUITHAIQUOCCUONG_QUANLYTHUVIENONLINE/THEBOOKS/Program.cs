﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DevExpress.UserSkins;
using DevExpress.Skins;
using System.Globalization;

namespace THEBOOKS
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// 

        public static String mLogin = ""; //giữ data trong textbox đăng nhập
        public static String mPassword = "";
        public static String mGroup = ""; //chỗ nào sáng lên hay mờ đi
        public static String mHoten = ""; //in ở cuối form cho biết trạng thái-> ai đang làm việc
        public static String fromReport = "";
        public static String requestCon = "http://thebtqcuong.online/quanlythuvien/public/api";
        //public static String requestCon = "http://localhost:8080/quanlythuvien/public/api";
        
        public static String connectString = "server=sql351.main-hosting.eu;user id=u602591515_cuong;password=Cc01687535522;persistsecurityinfo=True;database=u602591515_qltv";

        public static String chuanhoachuoi(string strInput)
        {
            strInput = strInput.Trim().ToLower();
            while (strInput.Contains("  "))
                strInput = strInput.Replace("  ", " ");
            string strResult = "";
            string[] arrResult = strInput.Split(' ');
            foreach (string item in arrResult)
                strResult += item.Substring(0, 1).ToUpper() + item.Substring(1) + " "; //lấy kí tự đầu tiên viết hoa + với ký tự sau
            return strResult.TrimEnd();
        }

        public static String FormatNumber(string currency)
        {
            CultureInfo culture = new CultureInfo("vi-VN");
            decimal value = decimal.Parse(currency, System.Globalization.NumberStyles.AllowThousands);
            return String.Format(culture, "{0:N0}", value);
        }

        public static String FormatDateTimeToString(DateTime dt)
        {
            return dt.ToString("dd/MM/yyyy");
        }

        //public static DateTime FormatStringToDate(String strDate)
        //{
        //    MessageBox.Show(strDate, "", MessageBoxButtons.OK);
        //    return DateTime.Parse("01/01/2020", new CultureInfo("en-CA"));
        //}

        public static String FormatLocalToServer(String dateStr)
        {
            string[] dateList = dateStr.Split('/');
            return dateList[2] + "/" + dateList[1] + "/" + dateList[0];
        }

        public static String Format_ServerToLocal(String dateStr)
        {
            string[] dateList = dateStr.Split('-');
            return dateList[2] + "/" + dateList[1] + "/" + dateList[0];
        }
        

        public static String UndoFormatNumber(string currency)
        {
            return currency.Replace(",","");
        }
        
        //Định nghĩa ký tự có dấu
        public static Char sourceAccent(Char text)
        {
            switch(text)
            {
                case 'A': case 'Á': case 'À': case 'Ạ': case 'Ả': case 'Ã':
                case 'Â': case 'Ấ': case 'Ầ': case 'Ẩ': case 'Ẫ': case 'Ậ':
                case 'Ă': case 'Ắ': case 'Ằ': case 'Ẳ': case 'Ẵ': case 'Ặ':
                    return 'A';
                case 'D': case 'Đ':
                    return 'D';
                case 'E': case 'É': case 'È': case 'Ẻ': case 'Ẹ': case 'Ẽ':
                case 'Ê': case 'Ế': case 'Ề': case 'Ể': case 'Ễ': case 'Ệ':
                    return 'E';
                case 'I':  case 'Í': case 'Ì': case 'Ỉ': case 'Ĩ': case 'Ị':
                    return 'I';
                case 'O': case 'Ó': case 'Ò': case 'Ỏ': case 'Õ': case 'Ọ':
                case 'Ô': case 'Ố': case 'Ồ': case 'Ổ': case 'Ỗ': case 'Ộ':
                case 'Ơ': case 'Ớ': case 'Ờ': case 'Ở': case 'Ỡ': case 'Ợ':
                    return 'O';
                case 'U': case 'Ú': case 'Ù': case 'Ụ': case 'Ủ': case 'Ũ':
                    return 'U';
                default:
                    return text;
            }
        }

        //chuyển đổi ký tự có dấu thành không dấu
        public static string removeAccent(String text)
        {
            string result = "";
            text = chuanhoachuoi(text).ToUpper();
            for(int i = 0; i < text.Length; i++)
            {
                result += sourceAccent(text[i]);
            }
            return result;
        }

        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            BonusSkins.Register();
            SkinManager.EnableFormSkins();
            Application.Run(new frmLogin());//frmLogin    frm_ReportDoanhThu
        }
    }

}
