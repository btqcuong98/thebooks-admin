﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Windows.Forms;
using ExcelDataReader;
using TheBooks_Model;
using System.Net.Http;
using Newtonsoft.Json;
using System.Globalization;

namespace THEBOOKS
{
    public partial class frmLoadFile : Form
    {
        DataSet dataset;
        List<Excel_Model> lstExcel = new List<Excel_Model>();
        List<NhaXuatBan_Model> lstNXB = new List<NhaXuatBan_Model>();
        int row = -1;

        public frmLoadFile()
        {
            InitializeComponent();
            loadTatCaNhaXuatBanToCombobox();
            btnSave.Enabled = false;
        }

        DataTableCollection dataTableCollection;

        private void btnImport_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog ofd = new OpenFileDialog() { Filter = "Excel Workbook|*.xlsx|Excel Workbook 97-2003|*.xls", ValidateNames = true })
            {
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    txbImport.Text = ofd.FileName;
                    try
                    {
                        using (var stream = File.Open(ofd.FileName, FileMode.Open, FileAccess.Read))
                        {
                            IExcelDataReader reader;
                            if (ofd.FilterIndex == 2)
                            {
                                reader = ExcelReaderFactory.CreateBinaryReader(stream);
                            }
                            else
                            {
                                reader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                            }
                 
                            dataset = reader.AsDataSet(new ExcelDataSetConfiguration()
                            {
                                ConfigureDataTable = (_) => new ExcelDataTableConfiguration()
                                {
                                    UseHeaderRow = true
                                }
                            });
                            //cmbImport.Items.Clear();
                            foreach (DataTable dt in dataset.Tables)
                            {
                                //cmbImport.Items.Add(dt.TableName);
                            }
                            reader.Close();
                        }
                    } catch(Exception exp)
                    {
                        MessageBox.Show("File đang mở !", "THONG BÁO", MessageBoxButtons.OK);
                        txbImport.Text = "";
                    }
                }
            }

        }

        private async void cmbImport_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private async void loadTatCaNhaXuatBanToCombobox()
        {
            try
            {
                var httpClient = new HttpClient();
                var url = Program.requestCon + "/nhaxb/laytatcanhaxuatban";
                var task = await httpClient.PostAsync(string.Format(url), null);
                task.EnsureSuccessStatusCode();
                var payload = task.Content.ReadAsStringAsync();

                lstNXB = JsonConvert.DeserializeObject<Parent_NhaXuatBan_Model>(payload.Result).data;

                if (lstNXB.Count > 0)
                {
                    cmbNXB.DataSource = lstNXB;
                    cmbNXB.DisplayMember = "Ten";
                    cmbNXB.ValueMember = "MaNXB";
                    txbNhaXB.Text = cmbNXB.SelectedValue.ToString().Trim();
                }
                else
                {
                    txbNhaXB.Text = "";
                    return;
                }

            }
            catch (Exception ex)
            {
                txbNhaXB.Text = "";
                return;
            }
        }


        //MARK : - BTN
        private async void btnSave_ClickAsync(object sender, EventArgs e)
        {
            if (txbMaPN.Text.ToString().Trim() != "" && txbNhaXB.Text.ToString().Trim() != "")
            {
                List<String> lstmLogin = new List<String>();
                lstmLogin.Add(Program.mLogin);

                List<String> lstNXB = new List<string>();
                lstNXB.Add(txbNhaXB.Text.ToString().Trim());

                List<String> lstMaPN = new List<string>();
                lstMaPN.Add(txbMaPN.Text.ToString().Trim());


                List<String> lstDauSach = new List<String>();
                List<String> lstTenSach = new List<String>();
                List<String> lstTheLoai = new List<String>();
                List<String> lstNam = new List<String>();
                List<String> lstMoTa = new List<String>();
                List<String> lstSoTrang = new List<String>();
                List<String> lstSoLuong = new List<String>();
                List<String> lstGia = new List<String>();

                for (int a = 0; a < lstExcel.Count; a++)
                {
                    lstDauSach.Add(lstExcel[a].MaDS);
                    lstTenSach.Add(lstExcel[a].TenSach);
                    lstTheLoai.Add(lstExcel[a].TenTLC);
                    lstNam.Add(lstExcel[a].NamXB);
                    lstMoTa.Add(lstExcel[a].MoTa);
                    lstSoTrang.Add(lstExcel[a].SoTrang);
                    lstSoLuong.Add(lstExcel[a].SoLuong);
                    lstGia.Add(Program.UndoFormatNumber(lstExcel[a].Gia));
                    //MessageBox.Show(""+ Program.UndoFormatNumber(lstExcel[a].Gia), "THONG BÁO", MessageBoxButtons.OK);
                }
                
                try
                {
                    NhapSach_Request request = new NhapSach_Request();
                    request.MaPN = lstMaPN;
                    request.MaTT = lstmLogin;
                    request.MaDS = lstDauSach;
                    request.TenSach = lstTenSach;
                    request.TenTLC = lstTheLoai;
                    request.NamXB = lstNam;
                    request.SoTrang = lstSoTrang;
                    request.MoTa = lstMoTa;
                    request.SoLuong = lstSoLuong;
                    request.Gia = lstGia;
                    request.MaNXB = lstNXB;

                    HttpContent val = new StringContent(JsonConvert.SerializeObject(request), System.Text.Encoding.UTF8, "application/json");
                    var url = Program.requestCon + "/nhanvientt/nhapsach";
                    using (var httpClient = new HttpClient())
                    {
                        var task = await httpClient.PostAsync(string.Format(url), val);
                        task.EnsureSuccessStatusCode();
                    }

                    MessageBox.Show("Nhập sách thành công !", "THÔNG BÁO", MessageBoxButtons.OK);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Nhập sách thất bại !" + ex.Message, "THÔNG BÁO", MessageBoxButtons.OK);
                    return;
                }
                
                //RESET AFTER INSERT
                lstExcel.Clear();
                dgvExcel.DataSource = new List<Excel_Model>();
                txbImport.Text = "";
                txbNhaXB.Text = cmbNXB.SelectedValue.ToString();
                txbMaPN.Text = "";
                btnSave.Enabled = false;
            }
            else
            {
                MessageBox.Show("Vui lòng nhập đầy đủ thông tin !", "THÔNG BÁO", MessageBoxButtons.OK);
                return;
            }
            
        }

        private void frmLoadFile_Load(object sender, EventArgs e)
        {

        }

        private String formatCurrency(String price)
        {
            CultureInfo cul = CultureInfo.GetCultureInfo("vi-VN");   // try with "en-US"
            string priceFormated = double.Parse("2000").ToString("#,###", cul.NumberFormat);
            return priceFormated;
        }
        
        private void dgvExcel_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            row = dgvExcel.CurrentCell.RowIndex;
        }

        private void cmbNXB_SelectedIndexChanged(object sender, EventArgs e)
        {
            txbNhaXB.Text = cmbNXB.SelectedValue.ToString();
        }

        private void brnReload_Click(object sender, EventArgs e)
        {
            loadTatCaNhaXuatBanToCombobox();
            btnSave.Enabled = true;
        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            int i = 0;
            foreach (DataRow row in dataset.Tables[0].Rows)
            {
                Excel_Model ex = new Excel_Model();
                if (i >= 6 && i <= (dataset.Tables[0].Rows.Count - 4))
                {
                    if (Convert.ToString(row[0]) != "")
                    {
                        ex.Stt = Convert.ToString(row[0]);
                        ex.MaDS = Convert.ToString(row[1]);
                        ex.TenSach = Convert.ToString(row[2]);
                        ex.TenTLC = Convert.ToString(row[3]);
                        ex.NamXB = Convert.ToString(row[4]);
                        ex.SoTrang = Convert.ToString(row[5]);
                        ex.MoTa = Convert.ToString(row[6]);
                        ex.SoLuong = Convert.ToString(row[7]);
                        ex.Gia = Program.FormatNumber(Convert.ToString(row[8]));

                        lstExcel.Add(ex);
                    }
                }

                i++;
            }
            dgvExcel.DataSource = lstExcel;
            btnSave.Enabled = true;
        }
    }
    

}
