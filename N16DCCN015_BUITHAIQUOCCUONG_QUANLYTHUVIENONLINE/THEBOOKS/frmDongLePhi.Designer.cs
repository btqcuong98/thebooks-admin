﻿namespace THEBOOKS
{
    partial class frmDongLePhi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnReport = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txbMaDG = new System.Windows.Forms.TextBox();
            this.cmbDocGia = new System.Windows.Forms.ComboBox();
            this.cmbHinhThuc = new System.Windows.Forms.ComboBox();
            this.txbMucGia = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnReport
            // 
            this.btnReport.BackColor = System.Drawing.Color.Maroon;
            this.btnReport.ForeColor = System.Drawing.Color.LavenderBlush;
            this.btnReport.Location = new System.Drawing.Point(46, 219);
            this.btnReport.Name = "btnReport";
            this.btnReport.Size = new System.Drawing.Size(500, 39);
            this.btnReport.TabIndex = 12;
            this.btnReport.Text = "LƯU ";
            this.btnReport.UseVisualStyleBackColor = false;
            this.btnReport.Click += new System.EventHandler(this.btnDongLePhi_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.ForeColor = System.Drawing.Color.Lime;
            this.label3.Location = new System.Drawing.Point(42, 160);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 21);
            this.label3.TabIndex = 10;
            this.label3.Text = "Hình Thức ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(154, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(319, 31);
            this.label2.TabIndex = 9;
            this.label2.Text = "ĐỌC GIẢ ĐÓNG LỆ PHÍ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.ForeColor = System.Drawing.Color.Lime;
            this.label4.Location = new System.Drawing.Point(65, 97);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 21);
            this.label4.TabIndex = 13;
            this.label4.Text = "Đọc Giả";
            // 
            // txbMaDG
            // 
            this.txbMaDG.Enabled = false;
            this.txbMaDG.Location = new System.Drawing.Point(410, 89);
            this.txbMaDG.Name = "txbMaDG";
            this.txbMaDG.Size = new System.Drawing.Size(136, 29);
            this.txbMaDG.TabIndex = 14;
            // 
            // cmbDocGia
            // 
            this.cmbDocGia.FormattingEnabled = true;
            this.cmbDocGia.Location = new System.Drawing.Point(160, 89);
            this.cmbDocGia.Name = "cmbDocGia";
            this.cmbDocGia.Size = new System.Drawing.Size(240, 29);
            this.cmbDocGia.TabIndex = 15;
            this.cmbDocGia.SelectedIndexChanged += new System.EventHandler(this.cmbDocGia_SelectedIndexChanged);
            // 
            // cmbHinhThuc
            // 
            this.cmbHinhThuc.FormattingEnabled = true;
            this.cmbHinhThuc.Items.AddRange(new object[] {
            "Đóng Lệ Phí Theo Năm",
            "Đóng Lệ Phí Theo Tháng"});
            this.cmbHinhThuc.Location = new System.Drawing.Point(160, 157);
            this.cmbHinhThuc.Name = "cmbHinhThuc";
            this.cmbHinhThuc.Size = new System.Drawing.Size(240, 29);
            this.cmbHinhThuc.TabIndex = 16;
            this.cmbHinhThuc.SelectedIndexChanged += new System.EventHandler(this.cmbHinhThuc_SelectedIndexChanged);
            // 
            // txbMucGia
            // 
            this.txbMucGia.Enabled = false;
            this.txbMucGia.Location = new System.Drawing.Point(410, 157);
            this.txbMucGia.Name = "txbMucGia";
            this.txbMucGia.Size = new System.Drawing.Size(136, 29);
            this.txbMucGia.TabIndex = 17;
            // 
            // frmDongLePhi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::THEBOOKS.Properties.Resources.images;
            this.ClientSize = new System.Drawing.Size(615, 290);
            this.Controls.Add(this.txbMucGia);
            this.Controls.Add(this.cmbHinhThuc);
            this.Controls.Add(this.cmbDocGia);
            this.Controls.Add(this.txbMaDG);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnReport);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmDongLePhi";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ĐÓNG LỆ PHÍ";
            this.Load += new System.EventHandler(this.frmDongLePhi_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnReport;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txbMaDG;
        private System.Windows.Forms.ComboBox cmbDocGia;
        private System.Windows.Forms.ComboBox cmbHinhThuc;
        private System.Windows.Forms.TextBox txbMucGia;
    }
}