﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TheBooks_Model;

namespace THEBOOKS
{
    public partial class frm_NhapSach : Form
    {
        List<TheLoaiCon_Model> lstTheLoai = new List<TheLoaiCon_Model>();
        List<NhaXuatBan_Model> lstNXB = new List<NhaXuatBan_Model>();

        public frm_NhapSach()
        {
            InitializeComponent();
            loadTheLoaiSachToCombobox();
            loadTatCaNhaXuatBanToCombobox();
            txbMaPN.Focus();
        }

        private void frm_NhapSach_Load(object sender, EventArgs e)
        {

        } 

        private Boolean validate()
        {
            if (txbMaPN.Text.ToString().Trim() == "")
            {
                MessageBox.Show("Vui lòng nhập Mã Phiếu Nhập !", "THÔNG BÁO", MessageBoxButtons.OK);
                txbMaPN.Focus();
                return false;
            }

            if (txbMaDS.Text.ToString().Trim() == "")
            {
                MessageBox.Show("Vui lòng nhập Mã Đầu Sách !", "THÔNG BÁO", MessageBoxButtons.OK);
                txbMaDS.Focus();
                return false;
            }

            if (txbTenS.Text.ToString().Trim() == "")
            {
                MessageBox.Show("Vui lòng nhập Tên Sách !", "THÔNG BÁO", MessageBoxButtons.OK);
                txbTenS.Focus();
                return false;
            }

            if (txbNXB.Text.ToString().Trim() == "")
            {
                MessageBox.Show("Vui lòng nhập Năm Xuất Bản !", "THÔNG BÁO", MessageBoxButtons.OK);
                txbNXB.Focus();
                return false;
            }

            if (txbSoTrang.Text.ToString().Trim() == "")
            {
                MessageBox.Show("Vui lòng nhập Số Trang !", "THÔNG BÁO", MessageBoxButtons.OK);
                txbSoTrang.Focus();
                return false;
            }

            if (txbMoTa.Text.ToString().Trim() == "")
            {
                MessageBox.Show("Vui lòng nhập Mô Tả !", "THÔNG BÁO", MessageBoxButtons.OK);
                txbMoTa.Focus();
                return false;
            }

            if (txbGia.Text.ToString().Trim() == "")
            {
                MessageBox.Show("Vui lòng nhập Giá !", "THÔNG BÁO", MessageBoxButtons.OK);
                txbGia.Focus();
                return false;
            }

            return true;
        }

        private async void loadTheLoaiSachToCombobox()
        {
            try
            {
                var httpClient = new HttpClient();
                var url = Program.requestCon + "/theloaicon/laythongtin";
                var task = await httpClient.PostAsync(string.Format(url), null);//.ConfigureAwait(false);
                task.EnsureSuccessStatusCode();
                var payload = task.Content.ReadAsStringAsync();
                
                lstTheLoai = JsonConvert.DeserializeObject<Parent_TheLoaiCon_Model>(payload.Result).data;

                cmbTheLoai.DataSource = lstTheLoai;
                cmbTheLoai.DisplayMember = "TenTLC";
                cmbTheLoai.ValueMember = "MaTLC";
                txbMaTL.Text = cmbTheLoai.SelectedValue.ToString();
            }
            catch (Exception ex)
            {
                return;
            }
        }

        private async void loadTatCaNhaXuatBanToCombobox()
        {
            try
            {
                var httpClient = new HttpClient();
                var url = Program.requestCon + "/nhaxb/laytatcanhaxuatban";
                var task = await httpClient.PostAsync(string.Format(url), null);
                task.EnsureSuccessStatusCode();
                var payload = task.Content.ReadAsStringAsync();

                lstNXB = JsonConvert.DeserializeObject<Parent_NhaXuatBan_Model>(payload.Result).data;

                if (lstNXB.Count > 0)
                {
                    cmbNXB.DataSource = lstNXB;
                    cmbNXB.DisplayMember = "Ten";
                    cmbNXB.ValueMember = "MaNXB";
                    txbMaNXB.Text = cmbNXB.SelectedValue.ToString().Trim();
                }
                else
                {
                    txbMaNXB.Text = "";
                    return;
                }

            }
            catch (Exception ex)
            {
                txbMaNXB.Text = "";
                return;
            }
        }

        private async void btnAddBook_Click(object sender, EventArgs e)
        {
            if(!validate()) {
                return;
            }

            try
            {
                Nhap1Sach_Request request = new Nhap1Sach_Request();
                request.MaPN = txbMaPN.Text.ToString().Trim();
                request.MaTT = Program.mLogin;
                request.MaDS = txbMaDS.Text.ToString().Trim();
                request.TenSach = txbTenS.Text.ToString().Trim();
                request.TenTLC = cmbTheLoai.Text.ToString().Trim();
                request.NamXB = txbNXB.Text.ToString().Trim();
                request.SoTrang = txbSoTrang.Text.ToString().Trim();
                request.MoTa = txbMoTa.Text.ToString().Trim();
                request.SoLuong = "1";
                request.Gia = txbGia.Text.ToString().Trim();
                request.MaNXB = txbMaNXB.Text.ToString().Trim();

                HttpContent val = new StringContent(JsonConvert.SerializeObject(request), System.Text.Encoding.UTF8, "application/json");
                var url = Program.requestCon + "/nhanvientt/nhap1sach";
                using (var httpClient = new HttpClient())
                {
                    var task = await httpClient.PostAsync(string.Format(url), val);
                    task.EnsureSuccessStatusCode();
                }

                DialogResult result =  MessageBox.Show("Nhập sách thành công !", "THÔNG BÁO", MessageBoxButtons.OK);

                if(result == DialogResult.OK)
                {
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Nhập sách thất bại !" + ex.ToString(), "THÔNG BÁO", MessageBoxButtons.OK);
                return;
            }
        }
        

        private void cmbTheLoai_MouseClick(object sender, MouseEventArgs e)
        {
            if(lstTheLoai.Count == 0)
            {
                loadTheLoaiSachToCombobox();
            }
        }

        private void cmbTheLoai_SelectedIndexChanged(object sender, EventArgs e)
        {
            txbMaTL.Text = cmbTheLoai.SelectedValue.ToString();
        }

        private void cmbNXB_MouseClick(object sender, MouseEventArgs e)
        {
            if(lstNXB.Count == 0)
            {
                loadTatCaNhaXuatBanToCombobox();
            }
        }

        private void cmbTenNXB_SelectedIndexChanged(object sender, EventArgs e)
        {
            txbMaNXB.Text = cmbNXB.SelectedValue.ToString().Trim();
        }
    }
}
