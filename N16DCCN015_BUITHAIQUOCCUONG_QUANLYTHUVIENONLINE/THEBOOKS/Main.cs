﻿using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using THEBOOKS.REPORT.FORM;
using THEBOOKS.REPORT.FORM._1_TuNgayDenNgay;
using THEBOOKS.REPORT.NhapSach;
using THEBOOKS.REPORT.SoLuotMuon;
using THEBOOKS.REPORT.TrangThaiSach;

namespace THEBOOKS
{
    public partial class Main : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public Main()
        {
            InitializeComponent();

            if(Program.mGroup == "Admin")
            {
                ribbonPage1.Visible = ribbonPage3.Visible = ribbonPage6.Visible = false;
                frmTaoTaiKhoan();
            }

            if(Program.mGroup == "ThuThu")
            {
                ribbonPage1.Visible = ribbonPage3.Visible = ribbonPage6.Visible = true;
                frmPhanCongGiaoSach();
            }

            statusInfo.Text = Program.mLogin + " - " + Program.mHoten;
        }

        private void Main_Load(object sender, EventArgs e)
        {
        }

        //func

        private Form CheckExists(Type ftype)
        {
            foreach (Form f in this.MdiChildren)
                if (f.GetType() == ftype)
                    return f;
            return null;
        }

        //MARK : -QUẢN LÝ

        //loadform
        private void frmPhanCongGiaoSach()
        {
            Form frm = this.CheckExists(typeof(frmPhanCongGiaoSach));
            if (frm != null)
            {
                frm.Close();
            }
            frmPhanCongGiaoSach f = new frmPhanCongGiaoSach();
            f.MdiParent = this;
            f.Show();
        }

        private void frmPhanCongThuSach()
        {
            Form frm = this.CheckExists(typeof(frmPhanCongThuSach));
            if (frm != null)
            {
                frm.Close();
            }
            frmPhanCongThuSach f = new frmPhanCongThuSach();
            f.MdiParent = this;
            f.Show();
        }

        

        private void frmTopDocGia()
        {
        }

        private void frmLoadFile()
        {
            Form frm = this.CheckExists(typeof(frmLoadFile));
            if (frm != null)
            {
                frm.Close();
            }
            frmLoadFile f = new frmLoadFile();
            f.MdiParent = this;
            f.Show();
        }

        private void frmDauSach()
        {
            Form frm = this.CheckExists(typeof(frmDauSach));
            if (frm != null)
            {
                frm.Close();
            }
            frmDauSach f = new frmDauSach();
            f.MdiParent = this;
            f.Show();
        }

        private void frmKhuVucPhanCong()
        {
            Form frm = this.CheckExists(typeof(frmPhanCongKhuVuc));
            if (frm != null)
            {
                frm.Close();
            }
            frmPhanCongKhuVuc f = new frmPhanCongKhuVuc();
            f.MdiParent = this;
            f.Show();
        }

        private void frmQuanLyDocGia()
        {
            Form frm = this.CheckExists(typeof(frmQuanLyDocGia));
            if (frm != null)
            {
                frm.Close();
            }
            frmQuanLyDocGia f = new frmQuanLyDocGia();
            f.MdiParent = this;
            f.Show();
        }

        private void frmTaoTaiKhoan()
        {
            Form frm = this.CheckExists(typeof(frmTaoTaiKhoan));
            if (frm != null)
            {
                frm.Close();
            }
            frmTaoTaiKhoan f = new frmTaoTaiKhoan();
            f.MdiParent = this;
            f.Show();
        }

        //report
        private void Report_frmTrangThaiSach()
        {
            XtraReport_TrangThaiSach rpt = new XtraReport_TrangThaiSach();
            ReportPrintTool print = new ReportPrintTool(rpt);
            print.ShowRibbonPreviewDialog();
        }

        private void frm_ReportLoiNhuan()
        {
            Form frm = this.CheckExists(typeof(frm_ReportLoiNhuan));
            if (frm != null)
            {
                frm.Close();
            }
            frm_ReportLoiNhuan f = new frm_ReportLoiNhuan();
            f.MdiParent = this;
            f.Show();
        }

        private void frm_ReportTanSuatSach()
        {
            Form frm = this.CheckExists(typeof(frm_ReportTanSuatSach));
            if (frm != null)
            {
                frm.Close();
            }
            frm_ReportTanSuatSach f = new frm_ReportTanSuatSach();
            f.MdiParent = this;
            f.Show();
        }

        private void frm_ReportMuonCuaDocGia()
        {
            Form frm = this.CheckExists(typeof(frm_ReportMuonCuaDocGia));
            if (frm != null)
            {
                frm.Close();
            }
            frm_ReportMuonCuaDocGia f = new frm_ReportMuonCuaDocGia();
            f.MdiParent = this;
            f.Show();
        }
        

        private void Report_frmTuNgayDenNgay()
        {
            frm_TuNgayDenNgay f = new frm_TuNgayDenNgay();
            f.Show();
        }

        private void frmBackground()
        {
            Form frm = this.CheckExists(typeof(frmBackground));
            if (frm != null)
            {
                frm.Close();
            }
            frmBackground f = new frmBackground();
            f.MdiParent = this;
            f.Show();
        }

        //action

        private void btnbarGiaoSach_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmPhanCongGiaoSach();
        }

        private void btnbarThuSach_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmPhanCongThuSach();
        }

        private void btnbarTopSach_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
           
        }

        private void btnbarTopDocGia_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmTopDocGia();
        }

        private void btnbarAllBook_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmDauSach();
        }

        private void btnbarLoadFile_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmLoadFile();
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmKhuVucPhanCong();
        }

        private void barButtonItem5_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmQuanLyDocGia();
        }

        private void barButtonItem9_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmTaoTaiKhoan();
        }
        
        private void barButtonItem10_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Program.fromReport = "XtraReport_TrangThaiSach";
            frmBackground();
            Report_frmTrangThaiSach();
        }

        private void barButtonItem11_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Program.fromReport = "XtraReport_NhapSach";
            frmBackground();
            Report_frmTuNgayDenNgay();
        }

        private void barButtonItem12_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frm_ReportMuonCuaDocGia();
        }

        private void barButtonItem13_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frm_ReportLoiNhuan();
        }

        private void barButtonItem14_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Program.fromReport = "XtraReport_ThuLePhi";
            frmBackground();
            Report_frmTuNgayDenNgay();
        }

        private void barButtonItem15_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frm_ReportTanSuatSach();
        }
    }
}
