﻿using System;
using System.Windows.Forms;
using System.Net.Http;
using Newtonsoft.Json;
using TheBooks_Model;

namespace THEBOOKS
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
            
        }
        
        private async void btnLogin_Click_1(object sender, EventArgs e)
        {
            groupBox1.Enabled = false;

            if (txbLogin.Text.ToString().Trim() == "" || txbPass.Text.ToString().Trim() == "")
            {
                MessageBox.Show("Login name và mật mã không được trống", "", MessageBoxButtons.OK);
                groupBox1.Enabled = true;
                return;
            }
            
            try
            {
                Login_Request request = new Login_Request();
                request.UserName = txbLogin.Text.Trim();
                request.Password = txbPass.Text.Trim();

                HttpContent val = new StringContent(JsonConvert.SerializeObject(request), System.Text.Encoding.UTF8, "application/json");
                var url = Program.requestCon + "/login/login";
                var httpClient = new HttpClient();
                var task = await httpClient.PostAsync(string.Format(url), val);

                task.EnsureSuccessStatusCode();

                var payload = task.Content.ReadAsStringAsync();

                Parent_Login_Model businessunits = JsonConvert.DeserializeObject<Parent_Login_Model>(payload.Result);

                if (businessunits.data.Count > 0)
                {
                    Program.mLogin = businessunits.data[0].MaND;
                    Program.mHoten = businessunits.data[0].HoTen;
                    Program.mGroup = businessunits.data[0].MaNQ;
                    
                    if (Program.mGroup == "ThuThu" || Program.mGroup == "Admin")
                    {
                        Main f = new Main();
                        f.ShowDialog();
                        groupBox1.Enabled = true;

                    } else
                    {
                        groupBox1.Enabled = true;
                        MessageBox.Show("Bạn không có quyền đăng nhập !", "", MessageBoxButtons.OK);
                    }
                    

                }
                else
                {
                    groupBox1.Enabled = true;
                    MessageBox.Show("Login thất bại !", "", MessageBoxButtons.OK);
                    return;

                }

            } catch(Exception ex)
            {
                    groupBox1.Enabled = true;
                    MessageBox.Show("Login thất bại !", "", MessageBoxButtons.OK);
                    return;

            }
            
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void txbLogin_TextChanged(object sender, EventArgs e)
        {

        }
    }
  
}
