﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using DevExpress.XtraReports.UI;
using MySql.Data.MySqlClient;
using THEBOOKS.REPORT._4_LoiNhuan;

namespace THEBOOKS
{
    public partial class frm_ReportLoiNhuan : Form
    {
        MySqlConnection connection = new MySqlConnection(Program.connectString);

        public frm_ReportLoiNhuan()
        {
            InitializeComponent();

            columnChart.Series["Lợi Nhuận"].Points.Clear();
            pieChartLoiNhuan.Series["Lợi Nhuận"].Points.Clear();

            dteFrom.Text = "01/01/2020";
            dteTo.Text = DateTime.Now.ToString("dd/MM/yyyy");
            chart();
        }

        public void chart()
        {
            try
            {
                DateTime from = DateTime.Parse(dteFrom.Text);
                DateTime to = DateTime.Parse(dteTo.Text);
                connection.Open();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "CALL `sp_Report_LoiNhuan`('" + Program.FormatLocalToServer(from.ToString("dd/MM/yyyy")) + "', '" + Program.FormatLocalToServer(to.ToString("dd/MM/yyyy")) + "')";
                MySqlDataReader reader;
                reader = cmd.ExecuteReader();
                
                while(reader.Read())
                {
                    string[] date = reader.GetString("Ngay").Split('-');
                    
                    columnChart.Series["Lợi Nhuận"].Points.AddPoint("Tháng " + date[0], reader.GetInt32("LoiNhuan"));

                    pieChartLoiNhuan.Series["Lợi Nhuận"].Points.AddPoint("Tháng " + date[0], reader.GetInt32("LoiNhuan"));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "", MessageBoxButtons.OK);
            }
            finally
            {
                if(connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            columnChart.Series["Lợi Nhuận"].Points.Clear();
            pieChartLoiNhuan.Series["Lợi Nhuận"].Points.Clear();
            chart();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DateTime from = Convert.ToDateTime(dteFrom.Text);
            DateTime to = Convert.ToDateTime(dteTo.Text);

            XtraReport_LoiNhuan rpt = new XtraReport_LoiNhuan(from, to);
            ReportPrintTool print = new ReportPrintTool(rpt);
            print.ShowPreview();
        }
    }
}
