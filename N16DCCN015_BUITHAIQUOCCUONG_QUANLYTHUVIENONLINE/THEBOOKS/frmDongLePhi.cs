﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TheBooks_Model;

namespace THEBOOKS
{
    public partial class frmDongLePhi : Form
    {
        List<DocGia_Model> lstDocGia = new List<DocGia_Model>();
        string hinhthuc = "-1";

        public frmDongLePhi()
        {
            InitializeComponent();
            loadTatCaDocGia();  
        }

        private async void loadTatCaDocGia()
        {
            try
            {
                var httpClient = new HttpClient();
                var url = Program.requestCon + "/nhanvientt/laytatcadocgia";
                var task = await httpClient.PostAsync(string.Format(url), null);//.ConfigureAwait(false);
                task.EnsureSuccessStatusCode();
                var payload = task.Content.ReadAsStringAsync();

                lstDocGia = JsonConvert.DeserializeObject<Parent_DocGia_Model>(payload.Result).data;
                cmbDocGia.DataSource = lstDocGia;
                cmbDocGia.DisplayMember = "HoTen";
                cmbDocGia.ValueMember = "MaDG";

                txbMaDG.Text = cmbDocGia.SelectedValue.ToString();
            }
            catch (Exception ex)
            {
                return;
            }
        }
        
        private void frmDongLePhi_Load(object sender, EventArgs e)
        {

        }

        private void cmbDocGia_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cmbDocGia.SelectedValue.ToString() != null)
            {
                txbMaDG.Text = cmbDocGia.SelectedValue.ToString();
            }
            
        }

        private void cmbHinhThuc_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cmbHinhThuc.Text.ToString() == "Đóng Lệ Phí Theo Năm")
            {
                hinhthuc = "2";
                txbMucGia.Text = "500,000";
            }
            else
            {
                hinhthuc = "1";
                txbMucGia.Text = "50,000";
            }
        }

        private async void btnDongLePhi_Click(object sender, EventArgs e)
        {
            try
            {
                DongLePhi_Request request = new DongLePhi_Request();
                request.MaDG = txbMaDG.Text;
                request.MaLP = hinhthuc;
                HttpContent val = new StringContent(JsonConvert.SerializeObject(request), System.Text.Encoding.UTF8, "application/json");
                var httpClient = new HttpClient();
                var url = Program.requestCon + "/docgia/donglephi";
                var task = await httpClient.PostAsync(string.Format(url), val);//.ConfigureAwait(false);
                task.EnsureSuccessStatusCode();
                var payload = task.Content.ReadAsStringAsync();

                MessageBox.Show("Đóng Lệ Phí Thành Công !", "THÔNG BÁO", MessageBoxButtons.OK);
                this.Hide();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Đóng Lệ Phí Thất Bại !" + "\n" + ex.Message, "THÔNG BÁO", MessageBoxButtons.OK);
                return;

            }
        }
    }
}
